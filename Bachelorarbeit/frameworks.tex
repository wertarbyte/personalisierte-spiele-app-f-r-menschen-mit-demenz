\chapter{Auswahl des Frameworks}
Als Entwickler kann viel Zeit in Kleinigkeiten verloren gehen, welche sich meist wiederholen. Besonders Spieleentwickler haben viele Gebiete auf denen sie arbeiten müssen. So müssen Texturen verwaltet, Figuren angezeigt, Töne abgespielt. Effekte animiert und Szenen zusammengestellt werden. Diese Arbeit kann jedoch erheblich erleichtert werden, mit der Nutzung eines passenden Frameworks. Dieses Dokument wurde mit LaTeX \cite{latex} geschrieben, damit sich der Author lediglich um den Inhalt kümmern muss. Formatierung, Bildpositionierung etc sind Angelegenheit von LateX. Bei der Spieleentwicklung sollte es ebenso laufen. Der Programmierer soll sich, wenn möglich, nur auf das Spiel konzentrieren können. Tiefere verwaltungstechnische Anforderungen soll dabei das Framework übernehmen.

Im folgenden Kapitel geht es deshalb um die Entscheidung das passende Framework finden zu können. Dabei werden erst einige mögliche Kandidaten vorgestellt und dann in wichtigen Punkten verglichen.

\section{Überblick}
Um jedes einmal richtig zu beleuchten folgt eine kurze Beschreibung der Frameworkes. 

\subsection{Sprite Kit}
Sprite Kit\cite{sprite_kit} ist ein von Apple eingeführtes Framework zur Realisierung von 2D Anwendungen für iOS Geräte. Es wurde mit dem Update iOS 7 und der Entwicklungsumgebung Xcode 5 veröffentlicht. Zuvor gab es direkt von Apple keine einfache Möglichkeit etwas aufwendigeres auf das Display bringen zu können. Die Programmierer mussten sich deshalb mit OpenGL ES oder anderen Frameworks zu helfen wissen. Nun müssen Entwickler nicht mehr aus dem internen Apple Entwickler Bereich heraus und können nativen Code mit Sprite Kit schreiben. 

Es unterstützt Sprites, Partikel, Animationen, Physiksimulation, Ton und Video. Ab Xcode 5 ist es sogar möglich Texturenpakete und Partikel im Designer bearbeiten zu können.
Jedes Spielobjekt ist in einem Baum organisiert und erbt von SKNode \cite{sknode}. Dadurch ist die Erstellung neuer Objekte ziemlich intuitiv gestaltet. So gibt es zum Beispiel SpriteNodes, welche Rechtecke und Texturen darstellen können, ShapeNode, welche unbestimmte Formen anzeigen und EmitterNodes für das Partikelsysteme. 
Jede SKNode besitzt eine Eigenschaft namens "physicsBody", welche wie erwartet aus unterschiedlichen Formen bestehen kann (Rechtecke, Kreise, Polygone, Pfade). Die API ist vergleichbar mit der Box2D \cite{box2d} Engine, welche bisher für solche Zwecke benutzt wurde.
Animationen sind dem von Cocos2D \cite{cocos2d} sehr nahe und bieten ebenso eine Menge an Möglichkeiten an. So können Animationen als Kette verknüpft und über zeitliche Parameter konfiguriert werden. Ebenso das Verhalten der Animationen ist beeinflussbar zumindest über linear, ausblenden, einblenden, ein- und ausblenden.

Direkt Entwicklerwerkzeuge von Apple zu beziehen hat einige Vorteile.  Durch die native Codenutzung wird die beste Geschwindigkeit erreicht. Es ist ein Ökosystem und alles ist auf einander abgestimmt. Xcode profitiert ebenfalls davon und kann das volle Potential von Sprite Kit nutzen.
Ein weiterer Grund wäre die Sicherstellung der Funktionalität für die Zukunft. Es ist nie sichergestellt, dass ein gewähltes Werkzeug vom Drittanbieter nach einem Update noch wie erwartet funktioniert. Es kann passieren, dass nichts mehr wie gehabt läuft und oder die Performance beeinträchtigt ist. Niemand weiß in solchen Fällen wie lang es dauert bis eine Fehlerbehebung für dieses Problem heraus gekommen ist.
Hinter Apple steckt natürlich ein viel höherer Druck ihr eigenes Werkzeug unter ihrer eigenen Umgebung dauerhaft stabil und funktional zu halten. Wird ein neues Feature für iOS oder OSX angekündigt ist es natürlich als erstes mit Sprite Kit nutzbar.

\subsection{Sparrow}
\begin{figure}[!htb]
\centerline{\includegraphics[width=0.1\textwidth]{bilder/frameworks/sparrow-logo}}
\caption{Sparrow Logo \cite{sparrow}.}
\label{frameworks_sparrow}
\end{figure}
Für Einsteiger wird meist das Framework Sparrow\cite{sparrow} empfohlen. Es ahmt die Funktionalitäten von Apple nach und bietet somit möglichst vollen Funktionsumfang. Als Zielplattform wird jedoch lediglich iOS angepeilt und die Umgebung kann nur als 2D Szene realisiert werden. Ebenfalls muss sich der Entwickler hier um keine OpenGL spezifischen Angelegenheiten kümmern, alles passiert unter der Haube.

Spielobjekte können hier ebenfalls als Eltern-Kind Beziehung verknüpft werden um einen Baum erzeugen zu können. 
Animationen und Aktionen gehen dabei den Baum durch. So können zum Beispiel Druckinteraktionen komplett durch den Baum iteriert werden.
Partikeleffekte machen ein Spiel erst richtig lebendig. In Sparrow sind diese sehr einfach zu gestalten:
\begin{lstlisting}[language=C++,
		caption=Partikelsystem in Sparrow,
		basicstyle={\ttfamily},
		keywordstyle={\color{blue}\ttfamily},
		stringstyle={\color{red}\ttfamily},
		commentstyle={\color{green}\ttfamily},
		breaklines=true
                  ]
SXParticleSystem *fire = [[SXParticleSystem alloc] initWithContentsOfFile:@"fire.pex"];
[fire start];
\end{lstlisting}
Texturen können in großen Mengen geladen werden. Ebenso ist es möglich neue Texturen aus bereits bestehenden zu erstellen und oder diesen mit einer weiteren Farbe einzufärben. Ebenfalls werden je nach aktiver Displayauflösung die passenden Texturen geladen.
Die Anzahl der zu erkennenden Finger auf dem Display ist ebenfalls variabel und ermöglicht so ein weiteres Spektrum an verfügbaren Spielinteraktionen. Die Nutzung dieser Interaktionspunkte ist hier ebenfalls ziemlich einfach gehalten:
\begin{lstlisting}[language=C++,
		caption=Multitouch in Sparrow,
		basicstyle={\ttfamily},
		keywordstyle={\color{blue}\ttfamily},
		stringstyle={\color{red}\ttfamily},
		commentstyle={\color{green}\ttfamily},
		breaklines=true
                  ]
- (void)onTouch:(SPTouchEvent *)event
{
    // hol dir alle bewegenden Beruehrungen
    NSSet *touches = [event touchesWithTarget:self andPhase:SPTouchPhaseMoved];
}
\end{lstlisting}
Als Besonderheit gilt noch zu sagen, dass Sparrow Open Source ist. Es steckt Herzblut vieler unabhängiger Entwickler dahinter. Benutzer können auch nach belieben den Code anpassen oder selbst Problembehebungen starten ohne auf ein Update warten zu müssen. Erweiterungen für dieses Framework zu schreiben ist deshalb ein wichtiger Bestandteil des Erfolges und ebenfalls einfach integriert:
\begin{lstlisting}[language=C++,
		caption=Erweiterbarkeit in Sparrow,
		basicstyle={\ttfamily},
		keywordstyle={\color{blue}\ttfamily},
		stringstyle={\color{red}\ttfamily},
		commentstyle={\color{green}\ttfamily},
		breaklines=true
                  ]
- (void)render:(SPRenderSupport *)support
{
    /* Dein OpenGL Code */
}
\end{lstlisting}

\subsection{Cocos2D}
\begin{figure}[!htb]
\centerline{\includegraphics[width=0.1\textwidth]{bilder/frameworks/cocos2d-logo}}
\caption{Cocos2d Logo \cite{cocos2d}.}
\label{frameworks_cocos2d}
\end{figure}
Cocos2d \cite{cocos2d} war bis vor SpriteKit das meist verbreitetste Framework, wenn es um 2D Anwendungen auf der iOS Plattform ging. Da SpriteKit nur mit Geräten von iOS 7 oder höher läuft, hat Cocos2d immer noch einen starken Anteil in der Entwicklergemeinschaft. Solange Entwickler noch Geräte unterstützen, welche nicht auf iOS 7 updaten können, wie das iPhone 3GS, wird sich an dieser Tatsache auch nichts ändern.
Cocos2d nutzt ebenfalls Objectiv-C Code und ist somit nur auf iOS Geräten lauffähig.

An herausstechenden Funktionalitäten lässt sich nicht viel neues sagen, was nicht schon in SpriteKit oder Sparrow beschrieben wurde. Cocos2d stand für SpriteKit, wie oben beschrieben, in Sachen Animationen als starkes Vorbild. Ebenso scheinen sich Sparrow und Cocos2d sehr stark von einander inspiriert zu haben. So findet sich hier ebenfalls die nützliche Aufteilung der Objekte in einer Baumstruktur wieder. Animationen nutzen diese Struktur, um bei einer Aktion des Elternteils, alle Kinder ebenfalls mit zu bewegen.

Jedoch stechen die Entwickler mit weiterer Software für ihre Frameworks heraus. So steht Cocos Studio für die Erstellung relevanter Spielinhalte zur Verfügung. Es ermöglicht die Nutzung von Werkzeugen für Oberflächen, Animationen, Szenen und Daten.  
Wie eben erwähnt wurden mehrere Frameworks entwickelt. Cocos2d hat aktuell fünf Forks und sticht somit in jede Mögliche Entwicklungsplattform durch. Die von Apple neu eingeführte Programmiersprache findet sich somit in der Abzweigung Cocos2D-Swift wieder. Ebenfalls existiert für XNA ein Zweig von Cocos2d und unterstützt die MonoGame Laufzeitumgebung. Dies ermöglicht das fertige Produkte auch unter Linux Systemen zum Laufen zu bekommen.
Die letzte Abspaltung, Cocos2d-x, wird im folgenden Unterkapitel behandelt.

Abschließend gilt auch hier noch zu sagen, dass es sich bei der Cocos2d Familie um ein Open Source Projekt handelt. Es existiert eine rege Gemeinschaft aus fähigen Entwicklern, welche sich in den Foren immer wieder für Verbesserungen und Erweiterungen aussprechen. Diese Bitten werden oftmals erhört und in einer nächsten Iteration integriert.

\subsection{Cocos2d-x}
\begin{figure}[!htb]
\centerline{\includegraphics[width=0.5\textwidth]{bilder/frameworks/cocos2dx-logo}}
\caption{Cocos2d-x Logo \cite{cocos2d-x}.}
\label{frameworks_cocos2dx}
\end{figure}
Cocos2d-x basiert auf den Grundlagen von Cocos2D. Als Programmiersprache wird C++ mit LUA oder Javascript bindings benutzt. Dies gibt Entwicklern mehr Freiraum, falls sie sich nicht im Objectiv-C oder Swift Bereich wohl fühlen. LUA findet in der modernen Spielentwicklung immer mehr Anklang und ermöglicht den Entwicklern kürzeren Code schreiben zu können. Es ist hier die Nutzung der Cocos Code IDE und das Debuggen während der Entwicklung möglich. Änderungen sind somit sofort sichtbar ohne die App erneut starten zu müssen. Die beschleunigt deutlich den Arbeitsfluss!

An Funktionalitäten lässt es sich also stark mit Cocos2d vergleichen. Das x im Namen soll für die "Cross Platform" Funktionalität stehen. Sprich im besten Fall wird die Anwendung einmal geschrieben und sie läuft auf allen unterstützten Systemen. Zu diesen zählen aktuell iOS, OSX, Android, Linux, Windows Phone und Windows. Zu beachten ist jedoch, dass Javascript bindings nicht auf dem Windows Phone laufen und hierfür Cocos2d-js genutzt werden muss.
Es ist möglich Java oder Objective-C/Swift an bestimmten Stellen nutzen zu können. Diese sollten sich jedoch nur auf OS spezifische Anwendungsfälle ausweiten um den Kern der App so portable wie möglich zu halten. Als Beispiel wären da die interne Bildgalerie, Google Play Dienste, Benachrichtigungen etc..
Auch wenn hier wieder “2D” im Namen steckt können einzelne 3D Elemente innerhalb der App genutzt und animiert werden. So können aufgesammelte Gegenstände plastischer dargestellt und animiert werden. Dies gibt Designern einfach sehr viel mehr Freiraum.

Cocos2d-x ist natürlich ebenfalls Open Source und besitzt, wie sein Elternteil, eine rege Gemeinschaft an Entwicklern. Die Entwicklung auf mehreren Systemen ist in der heutigen Zeit nicht mehr zu ignorieren. Deshalb findet Cocos2d-x mittlerweile eine standhafte Stellung in diesen Bereich.

\section{Vergleich der Frameworks}
Nach Abschluss der Beschreibungen, der einzelnen Frameworks, folgt nun eine Gegenüberstellung mit Vergleichskriterien. Diese werden vorerst noch einmal gesammelt und anschließen tabellarisch aufbereitet. Abschließend wird anhand dieser Tabelle das passende Framework, für diese Arbeit, ermittelt.

\subsection{Kriterien}
Die Aussage, dass es sich hierbei um eine iOS Entwicklung handelt sollte nicht die Programmiersprache festlegen. Deshalb ist das erste Kriterium die möglichen Programmiersprachen bzw. Skriptsprachen, welche die Frameworks unterstützten.

Für eine mögliche Weiterentwicklung ist ein zwanghafter Wechsel auf ein anderen Frameworks Auslöser für hohe kosten und eine lange Entwicklungsdauer. Aus diesen Grund bezieht sich der zweite Punkt auf die Plattformen, welche vom Framework von Haus aus unterstützt werden.

Die letzten Punkte listen die möglichen Funktionalitäten auf. Animation, Texturen, Sound, Partikel, Physics, Multitouch und Szenenverwaltung.

\subsection{Gegenüberstellung}
\begin{itemize}
  \item + = vorhanden
  \item - = nicht vorhanden
\end{itemize}

\begin{center}
\begin{tabular}{| l | l | l | l |  l |  l |  l |  l |  l |  l | p{5cm} |}
\hline
Framework & Programmiersprache & Platform \\ \hline
SpriteKit & Swift, Objectiv-C & iOS \\ \hline
Sparrow & Objectiv-C & iOS  \\ \hline
Cocos2D & Objective-C & iOS \\ \hline
Cocos2D-x & C++, LUA, Javascript & iOS, OSX, Android, Linux, Windows Phone und Windows \\ \hline
\end{tabular}
\end{center}

\begin{center}
\begin{tabular}{| l | l | l | l |  l |  l |  l |  l |  l |  l | p{5cm} |}
\hline
Framework & Animationen & Texturen & Sound & Partikel & Physics & Multitouch & Szenenverwaltung \\ \hline
SpriteKit & +   & +   & +   & +   & +   & +   & + \\ \hline
Sparrow & +   & +   & +   & +   & +   & +   & +  \\ \hline
Cocos2D & +   & +   & +   & +   & +   & +   & +  \\ \hline
Cocos2D-x & +   & +   & +   & +   & mit Box2D   & +   & + \\ \hline
\end{tabular}
\end{center}

\subsection{Die passende Wahl}
Der Funktionsumfang gleicht sich mehr oder weniger in allen Frameworks. Erst die spätere Implementierung wird Aufschluss über eine intelligente Umsetzung dieser Funktionen liefern können. NFA4 in Kapitel 3 sieht vor die Anwendung auch auf andere Systeme übertragen zu können. Und genau dieser Punkt ist ausschlaggebend dafür, dass Cocos2d-x für die Entwicklung der App ausgewählt wurde. Es unterstützt alle Systeme und minimiert somit den Mehraufwand bei einer späteren Umsetzung 