//
//  AboutScene.cpp
//  SnapGame
//
//  Created by Sascha on 02.02.15.
//
//

#include "AboutScene.h"
#include "SceneController.h"

USING_NS_CC;

bool AboutScene::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    auto backgroundSprite = Sprite::create("wood.png");
    
    auto s_visibleRect = Director::getInstance()->getOpenGLView()->getVisibleRect();
    float scaleY = s_visibleRect.size.height/backgroundSprite->getContentSize().height;
    float scaleX = s_visibleRect.size.width/backgroundSprite->getContentSize().width;
    if(scaleX>scaleY){
        backgroundSprite->setScale(scaleX);
    }else{
        backgroundSprite->setScale(scaleY);
    }
    backgroundSprite->setPosition(Point(s_visibleRect.origin.x+s_visibleRect.size.width/2, s_visibleRect.origin.y+s_visibleRect.size.height/2));
    this->addChild(backgroundSprite, -1);
    
    auto backButton = MenuItemImage::create("buttonBackNormal.png", "buttonBackPressed.png", CC_CALLBACK_1(AboutScene::back, this));
    auto gameMenu = Menu::create(backButton, NULL);
    gameMenu->setPosition(backButton->getContentSize().width/2, visibleSize.height - backButton->getContentSize().height/2);
    this->addChild(gameMenu,1);
    
    Label* augusteLabel = Label::createWithTTF("Auguste:\nAuguste Deter war die erste Patientin deren Symptome Dr. Aloys Alzheimer veranlasst haben, eine Krankheit zu beschreiben, die nach ihm benannt wurde, der Morbus Alzheimer, die häufigste dementielle Erkankung.", "LinLibertine_R.ttf", 40, Size(s_visibleRect.size.width/5*4,200));
    augusteLabel->setColor(Color3B::WHITE);
    MenuItemLabel* augusteItem = MenuItemLabel::create(augusteLabel);
    
    Label* gameLabel = Label::createWithTTF("Das Spiel:\nDas Spiel wurde unter der Leitung von Chefarzt der Klinik für Altersmedizin Herrn Dr. med. Konstantin Lekkos und seinem geriatischem Team im Helios Klinikum Hildesheim ins Leben gerufen und programmiert von Sascha Becker im Rahmen seiner Bachelorarbeit zum Studium der angewandten Informatik an der Hochschule Hannover. Die Idee im Zeitalter der Tablets war, Angehörige zu ermöglichen, mit Ihren dementiell erkrankten Angehörigen mobil, Hirnleistungstraining in jeder Umgebung, ob zu Hause, Pflegeeinrichtung oder Park durchzuführen. Natürlich ist die App auch gut in einer Klinik auf einem Tablet einsetzbar. Wir haben bewusst, auf ein zurückhaltendes Design, klare Linien und demenzgerechte Spiele geachtet. Die Bilddatenbank kann durch eigene Bilder, wie z.B. Bilder der Kinder oder Enkelkinder erweitert werden.", "LinLibertine_R.ttf", 40, Size(s_visibleRect.size.width/5*4,500));
    gameLabel->setColor(Color3B::WHITE);
    MenuItemLabel* gameItem = MenuItemLabel::create(gameLabel);
    
    Label* imagesLabel = Label::createWithTTF("Fotos:\nDas Spiel hat eine integrierte Bilddatenbank. Es gibt die Möglichkeit eigene Fotos in das Spiel zu integrieren, indem Sie über den Menüpunkt Bilder hinzufügen gehen und aus der Datenbank ihres Tablets Bilder integrieren. Wenn Sie ein Bild ausgesucht haben, geben Sie bitte dem Bild noch einem Namen, damit es in alles Spielen eingesetzt werden kann.", "LinLibertine_R.ttf", 40, Size(s_visibleRect.size.width/5*4,300));
    imagesLabel->setColor(Color3B::WHITE);
    MenuItemLabel* imagesItem = MenuItemLabel::create(imagesLabel);
    
    Menu* aboutList = Menu::create(augusteItem, gameItem, imagesItem, NULL);
    aboutList->alignItemsVerticallyWithPadding(10);
    
    //aboutList->setPosition(s_visibleRect.origin.x+s_visibleRect.size.width/2, s_visibleRect.origin.y+s_visibleRect.size.height/2);
    this->addChild(aboutList,2);
    
    return true;
}

cocos2d::Scene* AboutScene::createScene(){
    auto scene = Scene::create();
    auto layer = AboutScene::create();
    scene->addChild(layer);
    
    return scene;
}

void AboutScene::back(cocos2d::Ref* pSender){
    SceneController::getInstance()->showSettings(this);
}