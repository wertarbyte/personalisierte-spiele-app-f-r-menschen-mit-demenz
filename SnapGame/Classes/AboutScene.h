//
//  AboutScene.h
//  SnapGame
//
//  Created by Sascha on 02.02.15.
//
//

#ifndef __SnapGame__AboutScene__
#define __SnapGame__AboutScene__

#include "cocos2d.h"

class AboutScene : public cocos2d::Layer
{
public:
    static cocos2d::Scene* createScene();
    virtual bool init();
private:
    void back(Ref* pSender);
    CREATE_FUNC(AboutScene);
};

#endif /* defined(__SnapGame__AboutScene__) */
