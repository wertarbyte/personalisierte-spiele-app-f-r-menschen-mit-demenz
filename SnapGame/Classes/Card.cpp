#include "Card.h"

Card::Card(std::string name, std::string filename){
    this->name = name;
    this->filename = filename;
}

std::string Card::getName(){
    return name;
}

std::string Card::getFilename(){
    return filename;
}