#ifndef SnapGame_Card_h
#define SnapGame_Card_h

#include "cocos2d.h"

class Card
{
public:
    Card();
    Card(std::string name, std::string filename);
    std::string getName();
    std::string getFilename();
private:
    std::string name;
    std::string filename;
};


#endif
