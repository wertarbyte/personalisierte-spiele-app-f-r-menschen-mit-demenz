//
//  CustomImage.cpp
//  SnapGame
//
//  Created by Sascha Becker on 28.11.14.
//
//

#include "CustomImage.h"

CustomImage::CustomImage(){
    
}

CustomImage::~CustomImage(){
    
}

void CustomImage::setName(std::string name){
    this->name = name;
}

void CustomImage::setPath(std::string path){
    this->path = path;
}

std::string CustomImage::getName(){
    return name;
}

std::string CustomImage::getPath(){
    return path;
}

bool CustomImage::equals(CustomImage ci){
    return (this->getName() == ci.getName() && this->getPath() == ci.getPath());
}