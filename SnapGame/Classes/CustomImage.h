//
//  CustomImage.h
//  SnapGame
//
//  Created by Sascha Becker on 28.11.14.
//
//

#ifndef __SnapGame__CustomImage__
#define __SnapGame__CustomImage__

#include "cocos2d.h"

class CustomImage{
private:
    std::string name;
    std::string path;
public:
    CustomImage();
    ~CustomImage();
    void setName(std::string name);
    void setPath(std::string path);
    std::string getName();
    std::string getPath();
    bool equals(CustomImage ci);
};

#endif /* defined(__SnapGame__CustomImage__) */
