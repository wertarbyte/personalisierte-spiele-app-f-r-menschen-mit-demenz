#include "CustomImageGalleryImageLayer.h"
#include "CustomImagePool.h"
#include "CustomImage.h"
#include "GameConstants.h"
#include "DatabaseHelper.h"
#include "SceneController.h"

USING_NS_CC;

bool CustomImageGalleryImageLayer::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    
    width = GameConstants::CARD_TEXTURE_SIZE * maxCardsH + GameConstants::CARD_TEXTURE_SIZE * cardMargin * (maxCardsH - 1);
    int maxCardsV = 1 + CustomImagePool::getInstance()->getNumberOfCustomImages() / maxCardsH;
    height = GameConstants::CARD_TEXTURE_SIZE * maxCardsV + GameConstants::CARD_TEXTURE_SIZE * cardMargin * (maxCardsV - 1);
    
    initListener();
    refresh();
    
    return true;
}

void CustomImageGalleryImageLayer::initListener(){
    touchListener = EventListenerTouchOneByOne::create();
    touchListener->setSwallowTouches(true);
    
    touchListener->onTouchBegan = [](Touch* touch, Event* event){
        auto target = static_cast<Sprite*>(event->getCurrentTarget());
        
        //Get the position of the current point relative to the button
        Point locationInNode = target->convertToNodeSpace(touch->getLocation());
        Size s = target->getContentSize();
        Rect rect = Rect(0, 0, s.width, s.height);
        log("sprite began... ");
        
        //Check the click area
        if (rect.containsPoint(locationInNode))
        {
            target->setOpacity(180);
            log("sprite began... x = %f, y = %f", locationInNode.x, locationInNode.y);
            
            return true;
        }
        return false;
    };
    
    touchListener->onTouchEnded = [=](Touch* touch, Event* event){
        auto target = static_cast<Sprite*>(event->getCurrentTarget());
        
        //Get the position of the current point relative to the button
        Point locationInNode = target->convertToNodeSpace(touch->getLocation());
        Size s = target->getContentSize();
        Rect rect = Rect(0, 0, s.width, s.height);
        log("sprite end... ");
        
        target->setOpacity(255);
        
        //Check the click area
        if (rect.containsPoint(locationInNode))
        {
            log("sprite number %s onTouchesEnded.. ",target->getName().c_str());
            DatabaseHelper::getInstance()->deleteCustomImage(target->getName().c_str());
            SceneController::getInstance()->showSettings(nullptr);
        }
    };
}


float CustomImageGalleryImageLayer::getHeight(){
    return height;
}

float CustomImageGalleryImageLayer::getWidth(){
    return width;
}

void CustomImageGalleryImageLayer::refresh(){
    this->removeAllChildren();
    float x = 0;
    float y = 0;
    
    for (int i = 0; i < CustomImagePool::getInstance()->getNumberOfCustomImages(); i++) {
        if(i > 0 && i % maxCardsH == 0){
            x = 0;
            y += GameConstants::CARD_TEXTURE_SIZE + GameConstants::CARD_TEXTURE_SIZE * cardMargin;
        }
        
        Sprite* sp = Sprite::create();
        sp->setName(CustomImagePool::getInstance()->getCustomImageById(i).getName());
        sp->setTexture(cocos2d::FileUtils::sharedFileUtils()->getWritablePath() + CustomImagePool::getInstance()->getCustomImageById(i).getPath());
        sp->setPosition(x,y);
        _eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener->clone(), sp);
        
        this->addChild(sp, 1);
        
        x += GameConstants::CARD_TEXTURE_SIZE + GameConstants::CARD_TEXTURE_SIZE * cardMargin;
    }
}