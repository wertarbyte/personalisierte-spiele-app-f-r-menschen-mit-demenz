//
//  CustomImageGalleryImageLayer.h
//  SnapGame
//
//  Created by Sascha Becker on 09.12.14.
//
//

#ifndef __SnapGame__CustomImageGalleryImageLayer__
#define __SnapGame__CustomImageGalleryImageLayer__

#include "cocos2d.h"

class CustomImageGalleryImageLayer : public cocos2d::Layer{
private:
    float width, height;
    cocos2d::EventListenerTouchOneByOne* touchListener;
    float cardMargin = 0.05f;
    int maxCardsH = 5;
    
    void initListener();
    void refresh();
    
public:
    virtual bool init();
    float getWidth();
    float getHeight();
    
    CREATE_FUNC(CustomImageGalleryImageLayer);
};

#endif /* defined(__SnapGame__CustomImageGalleryImageLayer__) */
