#include "CustomImageGalleryScene.h"
#include "SceneController.h"
#include "CustomImageGalleryImageLayer.h"

USING_NS_CC;

Scene* CustomImageGalleryScene::createScene()
{
    auto scene = Scene::create();
    auto layer = CustomImageGalleryScene::create();
    scene->addChild(layer);
    return scene;
}

bool CustomImageGalleryScene::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    auto bg = Sprite::create("wood.png");
    
    auto s_visibleRect = Director::getInstance()->getOpenGLView()->getVisibleRect();
    float scaleY = s_visibleRect.size.height/bg->getContentSize().height;
    float scaleX = s_visibleRect.size.width/bg->getContentSize().width;
    if(scaleX>scaleY){
        bg->setScale(scaleX);
    }else{
        bg->setScale(scaleY);
    }
    bg->setPosition(Point(s_visibleRect.origin.x+s_visibleRect.size.width/2, s_visibleRect.origin.y+s_visibleRect.size.height/2));
    this->addChild(bg, -1);
    
    auto backButton = MenuItemImage::create("buttonBackNormal.png", "buttonBackNormal.png", CC_CALLBACK_1(CustomImageGalleryScene::back, this));
    auto gameMenu = Menu::create(backButton, NULL);
    gameMenu->setPosition(backButton->getContentSize().width/2, visibleSize.height - backButton->getContentSize().height/2);
    this->addChild(gameMenu,1);
    
    //add game layer
    CustomImageGalleryImageLayer* imageLayer = CustomImageGalleryImageLayer::create();
    cocos2d::log("Bounding Box %f, %f", imageLayer->getWidth(), imageLayer->getHeight());
    
    float scaleFactor = 1.0f;
    
    //defines margin around gameLayer
    float gameLayerMargin = 0.8f;
    
    if(imageLayer->getWidth() >= imageLayer->getHeight() && imageLayer->getWidth() >= cocos2d::Director::getInstance()->getWinSize().width * gameLayerMargin)
    {
        scaleFactor = (cocos2d::Director::getInstance()->getWinSize().width * gameLayerMargin / imageLayer->getWidth());
    }else if(imageLayer->getHeight() >= imageLayer->getWidth() && imageLayer->getWidth() && imageLayer->getHeight() >= cocos2d::Director::getInstance()->getWinSize().height){
        scaleFactor =  (cocos2d::Director::getInstance()->getWinSize().height * gameLayerMargin / imageLayer->getHeight());
    }
    
    imageLayer->setScale(scaleFactor);
    imageLayer->setPosition((cocos2d::Director::getInstance()->getWinSize().width/2 - imageLayer->getWidth()/2) * scaleFactor, (cocos2d::Director::getInstance()->getWinSize().height/2 - imageLayer->getHeight()/2) * scaleFactor);
    
    this->addChild(imageLayer, 1);
    
    /*int cardSize = 500;
    int maxCardsRow = visibleSize.width/cardSize;
    
    for (int i = 0; i < (CustomImageManager::getInstance()->getCustomImagesLength()+maxCardsRow)/maxCardsRow; i++) {
        for (int j = 0; j < maxCardsRow; j++) {
            if((i * maxCardsRow) + j < CustomImageManager::getInstance()->getCustomImagesLength()){
                CCLOG("Zeige Card %d in Reihe %i Spalte %i",i * maxCardsRow + j,i+1, j+1);
                CCLOG("%i",i * maxCardsRow + j);
                
                Card c = CustomImageManager::getInstance()->getCustomImage((i * maxCardsRow) + j);
                std::string t = c.getFilename();
                auto card = Sprite::create(cocos2d::FileUtils::sharedFileUtils()->getWritablePath() + t, cocos2d::Rect(0,0,1024, 1024));
                card->setScale(1.0 - (cardSize/1024.0), 1.0 - (cardSize/1024.0));
                card->setPosition(Vec2(j * cardSize + cardSize/2, visibleSize.height - (i * cardSize + cardSize/2)));
                this->addChild(card);
            }else
                break;
        }
    }*/
    
    //(visibleSize.width/3) - (visibleSize.width*0.1);
   /* auto cardMenu = Menu::create(NULL);
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 3; j++) {
            auto cardFront = Sprite::create("dotmatrix_0.jpg", cocos2d::Rect(0,0,cardSize, cardSize));
            std::string textArray[4] = {"dotmatrix_0.jpg", "slice_04.jpg", "slice_05.jpg", "slice_06.jpg"};
            int i = random(0, 3);
            std::string tex = textArray[i];
            cardFront->setTexture(tex);
            cardFront->setTextureRect(cocos2d::Rect(0,0,cardSize, cardSize));
            
            
            auto cardBack = Sprite::create("cardBackground.jpg", cocos2d::Rect(0,0,cardSize, cardSize));
            auto cardFrontMenuItem = MenuItemSprite::create(cardFront, cardFront, NULL);
            auto cardBackMenuItem = MenuItemSprite::create(cardBack, cardBack, NULL);
            auto cardToggle = MenuItemToggle::createWithCallback(NULL, cardBackMenuItem, cardFrontMenuItem, NULL);
            cardMenu->addChild(cardToggle);
        }
    }
    cardMenu->alignItemsInRows(2,2,2, NULL);
    this->addChild(cardMenu);*/
    
    return true;
}

void CustomImageGalleryScene::back(cocos2d::Ref* ref){
    SceneController::getInstance()->showSettings(this);
}

