#ifndef SnapGame_CUSTOMIMAGEGALLERYSCENE_h
#define SnapGame_CUSTOMIMAGEGALLERYSCENE_h

#include "cocos2d.h"

class CustomImageGalleryScene : public cocos2d::Layer
{
private:
    void back(cocos2d::Ref* ref);
public:
    static cocos2d::Scene* createScene();
    virtual bool init();
    
    CREATE_FUNC(CustomImageGalleryScene);
};

#endif
