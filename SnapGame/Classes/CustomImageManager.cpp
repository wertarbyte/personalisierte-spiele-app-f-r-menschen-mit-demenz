//
//  CustomImageManager.cpp
//  SnapGame
//
//  Created by Sascha Becker on 30.10.14.
//
//

#include "CustomImageManager.h"
#include "DatabaseHelper.h"
#include "SceneController.h"
#include <chrono>

std::string CustomImageManager::lastCustomImage;

CustomImageManager* CustomImageManager::instance = 0;

CustomImageManager::CustomImageManager(){
    CCLOG("CustomImageManager init");
}

CustomImageManager* CustomImageManager::getInstance(){
    if(instance == 0)
        instance = new CustomImageManager();
    return instance;
}

void CustomImageManager::addCustomImageFromSprite(cocos2d::Sprite* s){
    RenderTexture* renderTexture = RenderTexture::create(1024, 1024, Texture2D::PixelFormat::RGBA8888);
    s->setPosition(512, 512);
    renderTexture->begin();
    s->visit();
    renderTexture->end();
    
    // define exclusive fileName
    std::chrono::milliseconds ms = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now().time_since_epoch());
    std::stringstream ss;
    ss<<ms.count()<<".png";
    lastCustomImage = ss.str();
    
    //define ImageName
    imageName = s->getName();

    if(imageName == ""){
        imageName = lastCustomImage;
    }
    
    renderTexture->saveToFile(lastCustomImage, Image::Format::PNG, false, CC_CALLBACK_2(CustomImageManager::finishedSaving, this));
}

void CustomImageManager::addCustomImage(cocos2d::Image* img){
    auto tex = new Texture2D();
    tex->initWithImage(img);
    
    auto spr = Sprite::createWithTexture(tex);
    tex->release();
    
    addCustomImageFromSprite(spr);
}

void CustomImageManager::addCard(std::string name, std::string filename){
    Card c = Card(name, filename);
    cards.push_back(c);
}

Card CustomImageManager::getCustomImage(int i){
    return cards[i];
}

long CustomImageManager::getCustomImagesLength(){
    return cards.size();
}

void CustomImageManager::clearCards(){
    cards.clear();
}

void CustomImageManager::finishedSaving(cocos2d::RenderTexture *r, const std::string s){
  /*  Image* test = new Image();
    test->initWithImageFile(FileUtils::getInstance()->getWritablePath() + lastCustomImage);
    
    auto spritef = SpriteFrame::create(FileUtils::getInstance()->getWritablePath() + lastCustomImage, Rect(0,0,1024,1024));
    SpriteFrameCache::getInstance()->addSpriteFrame(spritef, "test");*/
    
    DatabaseHelper::getInstance()->insertCustomImage(imageName, lastCustomImage);
    SceneController::getInstance()->showSettings(NULL);
}