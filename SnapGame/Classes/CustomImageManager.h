//
//  CustomImageManager.h
//  SnapGame
//
//  Created by Sascha Becker on 30.10.14.
//
//

#ifndef SnapGame_CustomImageManager_h
#define SnapGame_CustomImageManager_h

#include "cocos2d.h"
#include "Card.h"
USING_NS_CC;

class CustomImageManager{
private:
    static CustomImageManager* instance;
    
    static std::string lastCustomImage;
    std::string imageName;
    std::vector<Card> cards;
    void finishedSaving(RenderTexture* r, const std::string s);
    
public:
    static CustomImageManager* getInstance();
    
    void addCustomImage(Image* img);
    void addCustomImageFromSprite(Sprite* s);
    void clearCards();
    void addCard(std::string name, std::string filename);
    Card getCustomImage(int i);
    long getCustomImagesLength();
    
protected:
    CustomImageManager();
};

#endif
