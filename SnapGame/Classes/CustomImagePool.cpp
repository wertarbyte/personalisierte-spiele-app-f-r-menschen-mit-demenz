//
//  CustomImagePool.cpp
//  SnapGame
//
//  Created by Sascha Becker on 28.11.14.
//
//

#include "CustomImagePool.h"

CustomImagePool* CustomImagePool::instance = 0;

CustomImagePool::CustomImagePool(){
    
}

CustomImagePool* CustomImagePool::getInstance(){
    if(instance == 0)
        instance = new CustomImagePool();
    return instance;
}

void CustomImagePool::addCustomImage(std::string name, std::string filepath){
    CustomImage c = CustomImage();
    c.setName(name);
    c.setPath(filepath);
    
    images.push_back(c);
}

void CustomImagePool::removeAllCustomImages(){
    images.clear();
}

long CustomImagePool::getNumberOfCustomImages(){
    return images.size();
}

CustomImage CustomImagePool::getCustomImageById(int i){
    return images[i];
}

CustomImage CustomImagePool::getRandomCustomImage(){
    int rand = cocos2d::rand_0_1() * images.size();
    return images[rand];
}

std::vector<CustomImage> CustomImagePool::getRandomCustomImages(int count){
    std::vector<CustomImage> randImages;
    
    if(images.size() > 0 && count <= images.size())
    {
        while (randImages.size() < count) {
            CustomImage ci = getRandomCustomImage();
            bool contains = false;
            
            for (int i=0; i < randImages.size(); i++) {
                if(ci.equals(randImages[i])){
                    contains = true;
                }
            }
            
            if(!contains){
                randImages.push_back(ci);
            }
        }
    }
    return randImages;
}
    
