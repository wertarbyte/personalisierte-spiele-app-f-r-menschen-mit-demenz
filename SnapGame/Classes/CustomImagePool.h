//
//  CustomImagePool.h
//  SnapGame
//
//  Created by Sascha Becker on 28.11.14.
//
//

#ifndef __SnapGame__CustomImagePool__
#define __SnapGame__CustomImagePool__

#include "CustomImage.h"

class CustomImagePool{
private:
    static CustomImagePool *instance;
    std::vector<CustomImage> images;
    
public:
    static CustomImagePool* getInstance();
    
    void addCustomImage(std::string name, std::string filepath);
    void removeAllCustomImages();
    CustomImage getCustomImageById(int i);
    long getNumberOfCustomImages();
    CustomImage getRandomCustomImage();
    std::vector<CustomImage> getRandomCustomImages(int count);
    
protected:
    CustomImagePool();
};

#endif /* defined(__SnapGame__CustomImagePool__) */
