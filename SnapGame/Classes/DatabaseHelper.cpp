#include "DatabaseHelper.h"
#include "CustomImageManager.h"
#include "CustomImagePool.h"

DatabaseHelper* DatabaseHelper::instanz = 0;

DatabaseHelper::DatabaseHelper(){
    dbPath = cocos2d::FileUtils::sharedFileUtils()->getWritablePath();
    dbPath.append("userdata.sqlite");
    result = sqlite3_open(dbPath.getCString(), &pDB);
    if(result != SQLITE_OK)
        CCLOG("Opening wrong, %d, MSG:%s",result, errMsg);
    CCLOG("result %d", result);
    
    //cleanup
    /*result = sqlite3_exec(pDB, "DROP TABLE importedImages", NULL, NULL, &errMsg);
    if(result != SQLITE_OK)
        CCLOG("Drop TABLE FAIL %d, MSG:%s", result, errMsg);*/
    
    //create table
    result = sqlite3_exec(pDB, "CREATE TABLE IF NOT EXISTS 'importedImages' ('name' TEXT NOT NULL PRIMARY KEY, 'filename' TEXT NOT NULL)", NULL, NULL, &errMsg);
    if(result != SQLITE_OK)
        CCLOG("CREATE TABLE FAIL %d, MSG:%s", result, errMsg);
    CCLOG("result %d", result);
    
    sqlite3_close(pDB);
    
    //fill cards
    getCustomImages();
}

DatabaseHelper* DatabaseHelper::getInstance(){
    
    if(instanz == 0)
        instanz = new DatabaseHelper();
    return instanz;
}

void DatabaseHelper::insertCustomImage(std::string name, std::string filename){
    result = sqlite3_open(dbPath.getCString(), &pDB);
    if(result != SQLITE_OK)
        CCLOG("Opening wrong, %d, MSG:%s",result, errMsg);
    
    std::stringstream ss;
    ss<<"INSERT INTO 'importedImages' (name,filename) VALUES ('"<<name<<"','"<<filename<<"');";
    
    result = sqlite3_exec(pDB, ss.str().c_str(), NULL, NULL, &errMsg);
    if (result != SQLITE_OK) {
        CCLOG("insert data failed %d, MSG:%s", result, errMsg);
    }
    
    sqlite3_close(pDB);
    
    CustomImageManager::getInstance()->clearCards(); //depricated
    CustomImagePool::getInstance()->removeAllCustomImages();
    getCustomImages();
}

void DatabaseHelper::deleteCustomImage(std::string name){
    result = sqlite3_open(dbPath.getCString(), &pDB);
    if(result != SQLITE_OK)
        CCLOG("Opening wrong, %d, MSG:%s",result, errMsg);
    
    std::stringstream ss;
    ss<<"DELETE FROM 'importedImages' WHERE name ='"<<name<<"';";
    
    result = sqlite3_exec(pDB, ss.str().c_str(), NULL, NULL, &errMsg);
    if (result != SQLITE_OK) {
        CCLOG("delete data failed %d, MSG:%s", result, errMsg);
    }
    
    sqlite3_close(pDB);
    
    CustomImagePool::getInstance()->removeAllCustomImages();
    getCustomImages();
}

void DatabaseHelper::getCustomImages(){
    result = sqlite3_open(dbPath.getCString(), &pDB);
    if(result != SQLITE_OK)
        CCLOG("Opening wrong, %d, MSG:%s",result, errMsg);
    
    result = sqlite3_exec(pDB, "SELECT * FROM 'importedImages'", callback, NULL, &errMsg);
    if (result != SQLITE_OK) {
        CCLOG("insert data failed %d, MSG:%s", result, errMsg);
    }
    
    sqlite3_close(pDB);
}

int DatabaseHelper::callback(void *data, int argc, char **argv, char **azColName){
    std::string name, filename;
    for(int i = 0; i < argc; i++){
        if(strcmp(azColName[i],"name"))
            name = argv[i];
        else if(strcmp(azColName[i],"filename"))
            filename = argv[i];
        
        CCLOG("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
    }
    CCLOG("\n");

    
    
    CustomImageManager::getInstance()->addCard(name, filename); //depricated
    CustomImagePool::getInstance()->addCustomImage(filename, name); //TODO change this later. Don't why this doesn't work as it should!!
    
    return 0;
}
