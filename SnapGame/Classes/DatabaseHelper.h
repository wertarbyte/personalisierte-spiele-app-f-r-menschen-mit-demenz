//
//  DatabaseHelper.h
//  SnapGame
//
//  Created by Sascha Becker on 26.10.14.
//
//

#ifndef SnapGame_DatabaseHelper_h
#define SnapGame_DatabaseHelper_h

#include <sqlite3.h>
#include "cocos2d.h"

using namespace cocos2d;

class DatabaseHelper{
    
private:
    static DatabaseHelper *instanz;
    sqlite3* pDB = NULL;
    char* errMsg = NULL;
    String sqlstr;
    int result;
    String dbPath;
    static int callback(void *data, int argc, char **argv, char **azColName);
    
public:
    static DatabaseHelper* getInstance();
    void insertCustomImage(std::string name, std::string filename);
    void getCustomImages();
    void deleteCustomImage(std::string name);
    
protected:
    DatabaseHelper();
};


#endif
