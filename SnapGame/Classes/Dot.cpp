//
//  Dot.cpp
//  SnapGame
//
//  Created by Sascha Becker on 04.12.14.
//
//

#include "Dot.h"

Dot::Dot(){
    
}

void Dot::setGlow(bool glow){
    this->glow = glow;
}

void Dot::setMark(bool marked){
    this->marked = marked;
}

void Dot::setDestroyed(bool destroyed){
    this->destroyed = destroyed;
}

bool Dot::hasGlow(){
    return glow;
}

bool Dot::isMarked(){
    return marked;
}

bool Dot::isDestroyed(){
    return destroyed;
}