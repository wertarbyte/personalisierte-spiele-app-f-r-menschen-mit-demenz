//
//  Dot.h
//  SnapGame
//
//  Created by Sascha Becker on 04.12.14.
//
//

#ifndef __SnapGame__Dot__
#define __SnapGame__Dot__

class Dot{
private:
    bool glow = false;
    bool marked = false;
    bool destroyed = false;
    
public:
    Dot();
    void setGlow(bool glow);
    void setMark(bool marked);
    void setDestroyed(bool destroyed);
    bool hasGlow();
    bool isMarked();
    bool isDestroyed();
};

#endif /* defined(__SnapGame__Dot__) */
