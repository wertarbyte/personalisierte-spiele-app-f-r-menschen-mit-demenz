#include "DotMatrixDifficultyScene.h"
#include "SceneController.h"
#include "bg.h"

USING_NS_CC;

Scene* DotMatrixDifficultyScene::createScene()
{
    auto scene = Scene::create();
    auto layer = DotMatrixDifficultyScene::create();
    scene->addChild(layer);
    return scene;
}

bool DotMatrixDifficultyScene::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    
    cocos2d::Layer *backGroundLayer = bg::create();
    this->addChild(backGroundLayer, -1);
    
    auto title = Sprite::create("title_chooseDifficulty.png");
    auto titleMenuItem = MenuItemSprite::create(title, NULL, NULL);
    auto titleMenu = Menu::create(titleMenuItem, NULL);
    titleMenu->setPosition(visibleSize.width/2,visibleSize.height - title->getContentSize().height/2);
    this->addChild(titleMenu,1);
    
    auto easySpriteNormal = Sprite::create("buttonEasyNormal.png");
    auto easySpritePressed = Sprite::create("buttonEasyPressed.png");
    auto easyMenuItem = MenuItemSprite::create(easySpriteNormal, easySpritePressed, CC_CALLBACK_1(DotMatrixDifficultyScene::clickEasy, this));
    
    auto moderateSpriteNormal = Sprite::create("buttonModerateNormal.png");
    auto moderateSpritePressed = Sprite::create("buttonModeratePressed.png");
    auto moderateMenuItem = MenuItemSprite::create(moderateSpriteNormal, moderateSpritePressed, CC_CALLBACK_1(DotMatrixDifficultyScene::clickModerate, this));
    
    auto hardSpriteNormal = Sprite::create("buttonHardNormal.png");
    auto hardSpritePressed = Sprite::create("buttonHardPressed.png");
    auto hardMenuItem = MenuItemSprite::create(hardSpriteNormal, hardSpritePressed, CC_CALLBACK_1(DotMatrixDifficultyScene::clickHard, this));
    
    auto gameListMenu = Menu::create(easyMenuItem, moderateMenuItem, hardMenuItem, NULL);
    gameListMenu->alignItemsVerticallyWithPadding(50);
    this->addChild(gameListMenu,1);
    
    return true;
}


void DotMatrixDifficultyScene::clickEasy(cocos2d::Ref* pSender){
    SceneController::getInstance()->startDotMatrix(4,2,3);
}

void DotMatrixDifficultyScene::clickModerate(cocos2d::Ref* pSender){
    SceneController::getInstance()->startDotMatrix(4,3,4);
}

void DotMatrixDifficultyScene::clickHard(cocos2d::Ref* pSender){
    SceneController::getInstance()->startDotMatrix(5,5,5);
}