#ifndef __SnapGame__DotMatrixDifficultyScene__
#define __SnapGame__DotMatrixDifficultyScene__

#include "cocos2d.h"

class DotMatrixDifficultyScene : public cocos2d::Layer{
public:
    static cocos2d::Scene* createScene();
    virtual bool init();
private:
    void clickEasy(cocos2d::Ref* pSender);
    void clickModerate(cocos2d::Ref* pSender);
    void clickHard(cocos2d::Ref* pSender);
    
    CREATE_FUNC(DotMatrixDifficultyScene);
};

#endif /* defined(__SnapGame__DotMatrixDifficultyScene__) */
