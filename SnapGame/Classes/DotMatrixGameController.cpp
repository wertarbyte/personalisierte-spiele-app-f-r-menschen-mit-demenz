//
//  DotMatrixGameController.cpp
//  SnapGame
//
//  Created by Sascha Becker on 04.12.14.
//
//

#include "DotMatrixGameController.h"
#include "CustomImagepool.h"
#include "SceneController.h"


DotMatrixGameController* DotMatrixGameController::instance = 0;

DotMatrixGameController::DotMatrixGameController(){

}

DotMatrixGameController* DotMatrixGameController::getInstance(){
    if(instance == 0)
        instance = new DotMatrixGameController();
    return instance;
}

int DotMatrixGameController::getNumberOfDotsPerRowAndColumn(){
    return NumberOfDotsPerRowAndColumn;
}

Dot DotMatrixGameController::getDot(int i){
    return dots[i];
}

int DotMatrixGameController::getState(){
    return state;
}

void DotMatrixGameController::setDelegate(DotMatrixGameDotLayer* del){
    this->del = del;
}

void DotMatrixGameController::setUIDelegate(DotMatrixGameUILayer* del){
    this->uiDel = del;
}

void DotMatrixGameController::initDots(int NumberOfDotsPerRowAndColumn, int GlowDotsPerRound, int options){
    dots.clear();
    this->NumberOfDotsPerRowAndColumn = NumberOfDotsPerRowAndColumn;
    this->GlowDotsPerRound = GlowDotsPerRound;
    
    dalliKlickOptions = CustomImagePool::getInstance()->getRandomCustomImages(options);
    int rand = cocos2d::rand_0_1() * dalliKlickOptions.size();
    dalliKlickImage = dalliKlickOptions[rand];
    
    for (int i = 0; i < NumberOfDotsPerRowAndColumn * NumberOfDotsPerRowAndColumn; i++) {
        Dot d = Dot();
        this->dots.push_back(d);
    }
    
    markDots();
}

void DotMatrixGameController::markDots(){
    // counter for already new marked dots
    int count = 0;
    if(dots.size() > 0 && GlowDotsPerRound <= dots.size())
    {
        while (count < GlowDotsPerRound) {
            int rand = cocos2d::rand_0_1() * NumberOfDotsPerRowAndColumn * NumberOfDotsPerRowAndColumn;
            
            if(!this->dots[rand].isDestroyed() && !dots[rand].isMarked()){
                dots[rand].setMark(true);
                dots[rand].setGlow(true);
                count ++;
            }
        }
    }
}

std::string DotMatrixGameController::getCustomImagePath(){
    return cocos2d::FileUtils::sharedFileUtils()->getWritablePath() + dalliKlickImage.getPath();
}

std::vector<std::string> DotMatrixGameController::getDalliKlickOptionNames(){
    std::vector<std::string> names;
    for (int i = 0; i < dalliKlickOptions.size(); i++) {
        names.push_back(dalliKlickOptions[i].getName());
    }
    
    return names;
}

void DotMatrixGameController::tapDot(int i){
    bool correct = false;
    
    //choose if tap was right or wrong
    if(dots[i].isMarked()){
        //user actually touched a marked dot. destroy dot
        dots[i].setDestroyed(true);
        correct = true;
    }else{
        //user tapped wrong dot
        correct = false;
    }
    
    // either hide glow effect or show them again to memorize
    for (int j = 0; j < dots.size(); j++) {
        if(dots[j].isMarked()){
            if(correct){
                dots[j].setGlow(false);
            }else{
                dots[j].setGlow(true);
            }
        }
    }
    
    checkState();
    
    del->update();
    uiDel->update();
}

void DotMatrixGameController::tapGuess(){
    state = 2;
    
    del->update();
    uiDel->update();
}

void DotMatrixGameController::tapName(std::string name){
    if(name == dalliKlickImage.getName()){
        cocos2d::log("Gewonnen richtiges Bild gewählt");
        state = 3;
    }else{
        // check if available dots for next round exist
        for (int i = 0; i < dots.size(); i++) {
            if(!dots[i].isDestroyed()){
                markDots();
                break;
            }
        }
        checkState();
    }
    
    del->update();
    uiDel->update();
}

void DotMatrixGameController::showEndScreen(){
    //kill instance and show endscreen
    instance = 0;
    SceneController::getInstance()->showGameOver(nullptr);
}

void DotMatrixGameController::checkState(){
    bool stillDotsToGo = false;
    
    for (int i = 0; i < dots.size(); i++) {
        if(dots[i].isMarked() && !dots[i].isDestroyed()){
            stillDotsToGo = true;
            break;
        }
    }
    
    if(stillDotsToGo){
        state = 0;
    }else{
        state = 1;
    }
}