#ifndef __SnapGame__DotMatrixGameController__
#define __SnapGame__DotMatrixGameController__

#include "Dot.h"
#include "CustomImage.h"
#include "DotMatrixGameDotLayer.h"
#include "DotMatrixGameUILayer.h"

class DotMatrixGameController{
private:
    static DotMatrixGameController *instance;
    CustomImage dalliKlickImage;
    std::vector<CustomImage> dalliKlickOptions;
    std::vector<Dot> dots;
    int NumberOfDotsPerRowAndColumn;
    int GlowDotsPerRound;
    DotMatrixGameDotLayer* del;
    DotMatrixGameUILayer* uiDel;
    int state = 0;
    
    void markDots();
    void checkState();
    
public:
    static DotMatrixGameController* getInstance();
    void initDots(int NumberOfDotsPerRowAndColumn, int GlowDotsPerRound, int options);
    int getNumberOfDotsPerRowAndColumn();
    std::string getCustomImagePath();
    Dot getDot(int i);
    int getState();
    
    void tapDot(int i);
    void tapGuess();
    void tapName(std::string name);
    void setDelegate(DotMatrixGameDotLayer* del);
    void setUIDelegate(DotMatrixGameUILayer* del);
    std::vector<std::string> getDalliKlickOptionNames();
    bool isGameOver();
    void showEndScreen();
    
protected:
    DotMatrixGameController();
};

#endif /* defined(__SnapGame__DotMatrixGameController__) */
