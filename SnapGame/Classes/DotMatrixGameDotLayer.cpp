//
//  DotMatrixGameDotLayer.cpp
//  SnapGame
//
//  Created by Sascha Becker on 04.12.14.
//
//

#include "DotMatrixGameDotLayer.h"
#include "DotMatrixGameController.h"
#include "GameConstants.h"

USING_NS_CC;

bool DotMatrixGameDotLayer::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    
    initListener();
    refreshDots();
    
    height = GameConstants::CARD_TEXTURE_SIZE * DotMatrixGameController::getInstance()->getNumberOfDotsPerRowAndColumn();
    width = height;
    
    DotMatrixGameController::getInstance()->setDelegate(this);
    
    return true;
}

float DotMatrixGameDotLayer::getHeight(){
    return height;
}

float DotMatrixGameDotLayer::getWidth(){
    return width;
}

void DotMatrixGameDotLayer::initListener(){
    touchListener = EventListenerTouchOneByOne::create();
    touchListener->setSwallowTouches(true);
    
    touchListener->onTouchBegan = [](Touch* touch, Event* event){
        auto target = static_cast<Sprite*>(event->getCurrentTarget());
        
        //Get the position of the current point relative to the button
        Point locationInNode = target->convertToNodeSpace(touch->getLocation());
        Size s = target->getContentSize();
        Rect rect = Rect(0, 0, s.width, s.height);
        log("sprite began... ");
        
        //Check the click area
        if (rect.containsPoint(locationInNode))
        {
            target->setTexture("dotPressed.png");
            log("sprite began... x = %f, y = %f", locationInNode.x, locationInNode.y);
            
            return true;
        }
        return false;
    };
    
    touchListener->onTouchEnded = [=](Touch* touch, Event* event){
        auto target = static_cast<Sprite*>(event->getCurrentTarget());
        
        //Get the position of the current point relative to the button
        Point locationInNode = target->convertToNodeSpace(touch->getLocation());
        Size s = target->getContentSize();
        Rect rect = Rect(0, 0, s.width, s.height);
        log("sprite end... ");
        
        target->setTexture("DotNormal.png");
        
        //Check the click area
        if (rect.containsPoint(locationInNode))
        {
            log("sprite number %s onTouchesEnded.. ",target->getName().c_str());
            
            int i = std::stoi(target->getName());
            DotMatrixGameController::getInstance()->tapDot(i);            
        }
    };
}

void DotMatrixGameDotLayer::refreshDots(){
    for (int i = 0; i < DotMatrixGameController::getInstance()->getNumberOfDotsPerRowAndColumn(); i++) {
        float y = i * GameConstants::CARD_TEXTURE_SIZE + GameConstants::CARD_TEXTURE_SIZE/2;
        
        for (int j = 0; j < DotMatrixGameController::getInstance()->getNumberOfDotsPerRowAndColumn(); j++) {
            if(!DotMatrixGameController::getInstance()->getDot(i*DotMatrixGameController::getInstance()->getNumberOfDotsPerRowAndColumn()+j).isDestroyed()){
                float x = j * GameConstants::CARD_TEXTURE_SIZE + GameConstants::CARD_TEXTURE_SIZE/2;
                Sprite* sp = Sprite::create();
                sp->setName(std::to_string(i*DotMatrixGameController::getInstance()->getNumberOfDotsPerRowAndColumn()+j));
                
                //create pulse animation
                auto glowAction = CallFunc::create([&]()
                                                      {
                                                          sp->setTexture("dotGlow.png");
                                                      });
                auto notGlowAction = CallFunc::create([&]()
                                                   {
                                                       sp->setTexture("dotNormal.png");
                                                   });
                
                if(DotMatrixGameController::getInstance()->getDot(i*DotMatrixGameController::getInstance()->getNumberOfDotsPerRowAndColumn()+j).hasGlow()){
                    sp->setTexture("dotGlow.png");
                }else{
                    sp->setTexture("DotNormal.png");
                }
                
                sp->setPosition(x,y);
                addChild(sp, 1);
                _eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener->clone(), sp);
            }
        }
    }
}

void DotMatrixGameDotLayer::refreshImageNameOptions(){
    float scaleFactor = width/GameConstants::CARD_TEXTURE_SIZE;
    
    Sprite* sp = Sprite::create();
    sp->setTexture("cardBlank.jpg");
    sp->setScale(scaleFactor);
    sp->setPosition(width/2, height/2);
    addChild(sp, 1);
    
    Menu* dkList = Menu::create();
    
    std::vector<std::string> names = DotMatrixGameController::getInstance()->getDalliKlickOptionNames();
    for (int i = 0; i < names.size(); i++) {
        Label* nameLabel = Label::createWithTTF(names[i], "LinLibertine_R.ttf", 50 * scaleFactor, Size(width,height/names.size()));
        nameLabel->setHorizontalAlignment(TextHAlignment::CENTER);
        nameLabel->setVerticalAlignment(TextVAlignment::CENTER);
        
        nameLabel->setColor(Color3B::BLACK);
        MenuItemLabel* name = MenuItemLabel::create(nameLabel, CC_CALLBACK_1(DotMatrixGameDotLayer::chooseName, this));
        name->setName(names[i]);
        
        dkList->addChild(name);
    }
    
    dkList->alignItemsVertically();
    dkList->setPosition(width/2, height/2);
    
    addChild(dkList, 2);
}

void DotMatrixGameDotLayer::chooseName(cocos2d::Ref* pSender){
    log("Name gewählt: %s",((MenuItemLabel*)pSender)->getName().c_str());
    DotMatrixGameController::getInstance()->tapName(((MenuItemLabel*)pSender)->getName().c_str());
}


void DotMatrixGameDotLayer::update(){
    this->removeAllChildren();
    //_eventDispatcher->removeAllEventListeners();
    initListener();
    
    if(DotMatrixGameController::getInstance()->getState() <= 1){
        log("Tap dots to see image");
        refreshDots();
    }else if(DotMatrixGameController::getInstance()->getState() == 2){
        log("Choose imagename");
        refreshImageNameOptions();
    }else if(DotMatrixGameController::getInstance()->getState() == 3){
        log("show image and wait for endscreen");
        auto showEndscreen = CallFunc::create([&]()
                                        {
                                            DotMatrixGameController::getInstance()->showEndScreen();
                                        });
        runAction(Sequence::create(DelayTime::create(3.0f), showEndscreen, nullptr));
    }
}

