//
//  DotMatrixGameDotLayer.h
//  SnapGame
//
//  Created by Sascha Becker on 04.12.14.
//
//

#ifndef __SnapGame__DotMatrixGameDotLayer__
#define __SnapGame__DotMatrixGameDotLayer__

#include "cocos2d.h"

class DotMatrixGameDotLayer : public cocos2d::Layer{
private:
    float width, height;
    void refreshDots();
    void refreshImageNameOptions();
    cocos2d::EventListenerTouchOneByOne* touchListener;
    void initListener();
    void chooseName(cocos2d::Ref* pSender);
    
public:
    virtual bool init();
    void update();
    float getWidth();
    float getHeight();
    
    CREATE_FUNC(DotMatrixGameDotLayer);
};

#endif /* defined(__SnapGame__DotMatrixGameDotLayer__) */
