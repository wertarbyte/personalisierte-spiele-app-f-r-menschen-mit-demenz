#include "DotMatrixGameImageLayer.h"
#include "DotMatrixGameController.h"
#include "GameConstants.h"

USING_NS_CC;

bool DotMatrixGameImageLayer::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    
    auto backgroundSprite = Sprite::create(DotMatrixGameController::getInstance()->getCustomImagePath());
    backgroundSprite->setPosition(GameConstants::CARD_TEXTURE_SIZE/2,GameConstants::CARD_TEXTURE_SIZE/2);
    this->addChild(backgroundSprite, 0);
    
    return true;
}

float DotMatrixGameImageLayer::getHeight(){
    return GameConstants::CARD_TEXTURE_SIZE;
}

float DotMatrixGameImageLayer::getWidth(){
    return GameConstants::CARD_TEXTURE_SIZE;
}