#ifndef __SnapGame__DotMatrixGameImageLayer__
#define __SnapGame__DotMatrixGameImageLayer__

#include "cocos2d.h"

class DotMatrixGameImageLayer : public cocos2d::Layer{
public:
    virtual bool init();
    float getWidth();
    float getHeight();
    
    CREATE_FUNC(DotMatrixGameImageLayer);
};

#endif /* defined(__SnapGame__DotMatrixGameImageLayer__) */
