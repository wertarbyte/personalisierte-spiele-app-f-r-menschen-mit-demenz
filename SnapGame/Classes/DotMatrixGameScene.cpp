#include "DotMatrixGameScene.h"
#include "bg.h"
#include "DotMatrixGameDotLayer.h"
#include "DotMatrixGameImageLayer.h"
#include "DotMatrixGameUILayer.h"

bool DotMatrixGameScene::init(){
    if(cocos2d::CCLayer::init()){
        
    }
    return true;
}

cocos2d::Scene* DotMatrixGameScene::createScene(){
    cocos2d::Scene *scene = cocos2d::Scene::create();
    
    //add background
    cocos2d::Layer *backGroundLayer = bg::create();
    scene->addChild(backGroundLayer, 0);
    
    //defines margin around gameLayer
    float gameLayerMargin = 0.7f;
    float scaleFactor = 1.0f;
    
    //add dot image layer
    DotMatrixGameImageLayer* dotImageLayer = DotMatrixGameImageLayer::create();
    cocos2d::log("Bounding Box %f, %f", dotImageLayer->getWidth(), dotImageLayer->getHeight());
    
    scaleFactor = (cocos2d::Director::getInstance()->getWinSize().height * gameLayerMargin / dotImageLayer->getHeight());
    
    dotImageLayer->setScale(scaleFactor);
    dotImageLayer->setPosition((cocos2d::Director::getInstance()->getWinSize().width/2 - dotImageLayer->getWidth()/2) * scaleFactor, (cocos2d::Director::getInstance()->getWinSize().height/2 - dotImageLayer->getHeight()/2) * scaleFactor);
    scene->addChild(dotImageLayer,1);
    
    //add dot layer
    DotMatrixGameDotLayer* dotLayer = DotMatrixGameDotLayer::create();
    cocos2d::log("Bounding Box %f, %f", dotLayer->getWidth(), dotLayer->getHeight());
    
    scaleFactor = (cocos2d::Director::getInstance()->getWinSize().height * gameLayerMargin / dotLayer->getHeight());
    
    dotLayer->setScale(scaleFactor);
    dotLayer->setPosition((cocos2d::Director::getInstance()->getWinSize().width/2 - dotLayer->getWidth()/2) * scaleFactor, (cocos2d::Director::getInstance()->getWinSize().height/2 - dotLayer->getHeight()/2) * scaleFactor);

    scene->addChild(dotLayer,2);
    
    //add UI Layer
    DotMatrixGameUILayer* uiLayer = DotMatrixGameUILayer::create();
    scene->addChild(uiLayer,3);
    
    return scene;
}