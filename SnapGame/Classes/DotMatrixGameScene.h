//
//  DotMatrixGameScene.h
//  SnapGame
//
//  Created by Sascha Becker on 04.12.14.
//
//

#ifndef __SnapGame__DotMatrixGameScene__
#define __SnapGame__DotMatrixGameScene__

#include "cocos2d.h"


class DotMatrixGameScene : public cocos2d::Layer{
public:
    virtual bool init();
    static cocos2d::Scene* createScene();
    
    CREATE_FUNC(DotMatrixGameScene);
};

#endif /* defined(__SnapGame__DotMatrixGameScene__) */
