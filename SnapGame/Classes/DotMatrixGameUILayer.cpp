//
//  DotMatrixGameUILayer.cpp
//  SnapGame
//
//  Created by Sascha on 26.01.15.
//
//

#include "DotMatrixGameUILayer.h"
#include "DotMatrixGameController.h"

USING_NS_CC;

bool DotMatrixGameUILayer::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    
    DotMatrixGameController::getInstance()->setUIDelegate(this);
    
    title = Sprite::create("title_dotMatrixGame_touchRedPoint.png");
    auto titleMenuItem = MenuItemSprite::create(title, NULL, NULL);
    auto titleMenu = Menu::create(titleMenuItem, NULL);
    titleMenu->setPosition(Director::getInstance()->getWinSize().width/2,Director::getInstance()->getWinSize().height - title->getContentSize().height/2);
    this->addChild(titleMenu,1);
    
    auto actionSpriteNormal = Sprite::create("buttonSolveNormal.png");
    auto actionSpritePressed = Sprite::create("buttonSolvePressed.png");
    actionMenuItem = MenuItemSprite::create(actionSpriteNormal, actionSpritePressed, CC_CALLBACK_1(DotMatrixGameUILayer::clickAction, this));
    
    auto gameListMenu = Menu::create(actionMenuItem, NULL);
    gameListMenu->setPosition(Director::getInstance()->getWinSize().width/2,actionSpriteNormal->getContentSize().height/2 + 10);
    this->addChild(gameListMenu,1);
    actionMenuItem->setVisible(false);
    return true;
}

void DotMatrixGameUILayer::clickAction(cocos2d::Ref* pSender){
    DotMatrixGameController::getInstance()->tapGuess();
}

void DotMatrixGameUILayer::update(){
    if(DotMatrixGameController::getInstance()->getState() == 0){
        title->setTexture("title_dotMatrixGame_touchRedPoint.png");
        actionMenuItem->setVisible(false);
    }else if(DotMatrixGameController::getInstance()->getState() == 1){
        title->setTexture("title_dotMatrixGame_canSolve.png");
        actionMenuItem->setVisible(true);
    }else if(DotMatrixGameController::getInstance()->getState() == 2){
        title->setTexture("title_dotMatrixGame_chooseName.png");
        actionMenuItem->setVisible(false);
    }else if(DotMatrixGameController::getInstance()->getState() == 3){
        title->setVisible(false);
        actionMenuItem->setVisible(false);
    }
}