//
//  DotMatrixGameUILayer.h
//  SnapGame
//
//  Created by Sascha on 26.01.15.
//
//

#ifndef __SnapGame__DotMatrixGameUILayer__
#define __SnapGame__DotMatrixGameUILayer__

#include "cocos2d.h"

class DotMatrixGameUILayer : public cocos2d::Layer{
private:
    cocos2d::Sprite* title;
    cocos2d::MenuItemSprite* actionMenuItem;
    void clickAction(cocos2d::Ref* pSender);
    
public:
    virtual bool init();
    void update();
    
    CREATE_FUNC(DotMatrixGameUILayer);
};

#endif /* defined(__SnapGame__DotMatrixGameUILayer__) */
