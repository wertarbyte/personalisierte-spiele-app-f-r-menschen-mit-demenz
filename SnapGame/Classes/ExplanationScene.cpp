//
//  ExplanationScene.cpp
//  SnapGame
//
//  Created by Sascha on 02.02.15.
//
//

#include "ExplanationScene.h"
#include "SceneController.h"

USING_NS_CC;

bool ExplananationScene::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    auto backgroundSprite = Sprite::create("wood.png");
    
    auto s_visibleRect = Director::getInstance()->getOpenGLView()->getVisibleRect();
    float scaleY = s_visibleRect.size.height/backgroundSprite->getContentSize().height;
    float scaleX = s_visibleRect.size.width/backgroundSprite->getContentSize().width;
    if(scaleX>scaleY){
        backgroundSprite->setScale(scaleX);
    }else{
        backgroundSprite->setScale(scaleY);
    }
    backgroundSprite->setPosition(Point(s_visibleRect.origin.x+s_visibleRect.size.width/2, s_visibleRect.origin.y+s_visibleRect.size.height/2));
    this->addChild(backgroundSprite, -1);
    
    auto backButton = MenuItemImage::create("buttonBackNormal.png", "buttonBackPressed.png", CC_CALLBACK_1(ExplananationScene::back, this));
    auto gameMenu = Menu::create(backButton, NULL);
    gameMenu->setPosition(backButton->getContentSize().width/2, visibleSize.height - backButton->getContentSize().height/2);
    this->addChild(gameMenu,1);
    
    Label* findLabel = Label::createWithTTF("Bilder finden:\nDurch Drücken auf einen Kasten wird Ihnen ein Bild angezeigt. Ihre Aufgabe ist es das gleiche Bild zu finden. Wenn Sie das Bild richtig gefunden haben, verschwindet das Paar. Bei falschem Drücken bleiben die Bilder für ca. 3 Sekunden zu sehen, damit ausreichend Zeit ist, sich die Positionen zu merken. Es gibt drei verschiedene Schwierigkeitsstufen, die sich in der Menge der Karten unterscheiden.", "LinLibertine_R.ttf", 40, Size(s_visibleRect.size.width/5*4,300));
    findLabel->setColor(Color3B::WHITE);
    MenuItemLabel* findItem = MenuItemLabel::create(findLabel);
    
    Label* guessLabel = Label::createWithTTF("Bilder raten:\nHier sollen sich rote Punkte gemerkt sowie entsprechend gedrückt werden und im Verlauf das erschienene Bild erraten werden. Nach dem Drücken eines roten Punktes, verschwinden die weiteren. Eine Reihenfolge muss nicht eingehalten werden. Wenn ein falscher Punkt gedrückt wird, erscheinen erneut die roten Punkte. Die roten Punkte bleiben so lange, bis ein Punkt gedrückt wird. Nach dem richtigen Drücken der roten Punkte erscheint jeweils ein Abschnitt eines Gesamtbildes. Nach dem Drücken aller erschienenen roten Punkte, taucht ein Feld auf mit dem Hinweis “Lösen”. Nach dem Drücken des Hinweis erscheinen mehrere Antworten mit einer richtigen Lösungsmöglicheit. Sollte die richtige Lösung gedrückt werden erscheint das richtig erratene Bild. Wenn eine falsche Lösung gedrückt wurde, geht das Spiel weiter und es erscheinen wieder rote Punkte. Das Spiel geht so lange bis das Bild richtig erraten werden konnte. Es gibt mehrere Schwierigkeitsstufen die sich von der Anzahl der angezeigten roten Punkte und der Anzahl der Kästen unterscheiden.", "LinLibertine_R.ttf", 40, Size(s_visibleRect.size.width/5*4,600));
    guessLabel->setColor(Color3B::WHITE);
    MenuItemLabel* guessItem = MenuItemLabel::create(guessLabel);
    
    Label* rememberLabel = Label::createWithTTF("Bilder merken:\nHier soll der Spieler, sich die anfänglich gezeigten mit einem grünen Haken markierten Bilder, merken und im Verlauf unter anderen gezeigten Bilder erkennen. Das richtige Bild muss gedrückt werden und wenn ein Bild richtig erkannt wurde, wird dies durch ein grünen Haken als richtig dargestellt. Bei falsch gedrücktem Bild erscheint ein rotes Kreuz. Hier gibt es drei Schwierigkeitsstufen, die sich von der Anzahl der zu merkenden Bilder, von der Anzahl der angezeigten Bilder und der möglichen Runden unterscheidet.", "LinLibertine_R.ttf", 40, Size(s_visibleRect.size.width/5*4,400));
    rememberLabel->setColor(Color3B::WHITE);
    MenuItemLabel* rememberItem = MenuItemLabel::create(rememberLabel);
    
    Menu* aboutList = Menu::create(findItem, guessItem, rememberItem, NULL);
    aboutList->alignItemsVerticallyWithPadding(10);
    
    //aboutList->setPosition(s_visibleRect.origin.x+s_visibleRect.size.width/2, s_visibleRect.origin.y+s_visibleRect.size.height/2);
    this->addChild(aboutList,2);
    
    return true;
}

cocos2d::Scene* ExplananationScene::createScene(){
    auto scene = Scene::create();
    auto layer = ExplananationScene::create();
    scene->addChild(layer);
    
    return scene;
}

void ExplananationScene::back(cocos2d::Ref* pSender){
    SceneController::getInstance()->showSettings(this);
}