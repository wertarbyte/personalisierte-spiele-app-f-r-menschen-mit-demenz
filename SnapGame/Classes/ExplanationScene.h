//
//  ExplanationScene.h
//  SnapGame
//
//  Created by Sascha on 02.02.15.
//
//

#ifndef __SnapGame__ExplanationScene__
#define __SnapGame__ExplanationScene__

#include "cocos2d.h"

class ExplananationScene : public cocos2d::Layer
{
public:
    static cocos2d::Scene* createScene();
    virtual bool init();
private:
    void back(Ref* pSender);
    CREATE_FUNC(ExplananationScene);
};

#endif /* defined(__SnapGame__ExplanationScene__) */
