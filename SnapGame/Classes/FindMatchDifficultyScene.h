//
//  FindMatchDifficultyScene.h
//  SnapGame
//
//  Created by Sascha on 07.03.15.
//
//

#ifndef __SnapGame__FindMatchDifficultyScene__
#define __SnapGame__FindMatchDifficultyScene__

#include "cocos2d.h"

class FindMatchDifficultyScene : public cocos2d::Layer{
public:
    static cocos2d::Scene* createScene();
    virtual bool init();
private:
    void clickEasy(cocos2d::Ref* pSender);
    void clickModerate(cocos2d::Ref* pSender);
    void clickHard(cocos2d::Ref* pSender);
    
    CREATE_FUNC(FindMatchDifficultyScene);
};

#endif /* defined(__SnapGame__FindMatchDifficultyScene__) */
