//
//  FindMatchGameController.cpp
//  SnapGame
//
//  Created by Sascha on 07.03.15.
//
//

#include "FindMatchGameController.h"
#include "CustomImagePool.h"
#include "CustomImage.h"

FindMatchGameController* FindMatchGameController::instance = 0;

FindMatchGameController::FindMatchGameController(){
    
}

FindMatchGameController* FindMatchGameController::getInstance(){
    if(instance == 0)
        instance = new FindMatchGameController();
    return instance;
}

void FindMatchGameController::initCards(int numberOfCardsX, int numberOfCardsY){
    cards.clear();
    
    this->numberOfCardsX = numberOfCardsX;
    this->numberOfCardsY = numberOfCardsY;
    
    int numberOfCards = this->numberOfCardsX * this->numberOfCardsY;
    
    //get random Images
    std::vector<CustomImage> customImages = CustomImagePool::getInstance()->getRandomCustomImages(numberOfCards/2);
    
    //duplicate cards for memory logic
    for (int i = 0; i < numberOfCards/2; i++) {
        customImages.insert(customImages.end(), customImages[i]);
    }
    
    //shuffle cards
    for (int i = 0; i < 100; i++) {
        
        int rand1 = cocos2d::rand_0_1() * numberOfCards;
        int rand2 = cocos2d::rand_0_1() * numberOfCards;
        
        CustomImage temp = customImages[rand1];
        customImages[rand1] = customImages[rand2];
        customImages[rand2] = temp;
    }
    
    //weise customimage den Karten zu
    for (int i = 0; i < numberOfCards; i++) {
        MemoryCard mc = MemoryCard(customImages[i]);
        this->cards.push_back(mc);
    }
}

int FindMatchGameController::getNumberOfCardsX(){
    return numberOfCardsX;
}

int FindMatchGameController::getNumberOfCardsY(){
    return numberOfCardsY;
}

void FindMatchGameController::setDelegate(FindMatchGameLayer* del){
    this->del = del;
}

MemoryCard FindMatchGameController::getCard(int i){
    return cards[i];
}

void FindMatchGameController::tapCard(int i){
    //count flipped cards to prevent more than 2 open cards at the same time
    int count = 0;
    for (int i = 0; i < getNumberOfCardsY() * getNumberOfCardsX(); i++) {
        if(cards[i].isFlipped() && !cards[i].hasMatch()){
            count++;
        }
    }
    
    //flip selected card
    if(count < 2 && !cards[i].hasMatch()){
        cards[i].flip();
        del->update();
    }
}


void FindMatchGameController::finalizeRound(){
    bool changed;
    
    //mark matches
    for (int i = 0; i < getNumberOfCardsY() * getNumberOfCardsX(); i++) {
        if(!cards[i].hasMatch() && cards[i].isFlipped()){
            for (int j = i+1; j < getNumberOfCardsY() * getNumberOfCardsX(); j++) {
                if(!cards[j].hasMatch() && cards[j].isFlipped()){
                    if(cards[i].getName() == cards[j].getName()){
                        cards[i].foundMatch();
                        cards[j].foundMatch();
                        changed = true;
                        break;
                    }
                }
            }
        }
    }
    
    
    for (int i = 0; i < getNumberOfCardsY() * getNumberOfCardsX(); i++) {
        if(cards[i].isFlipped() && !cards[i].hasMatch()){
            for (int j = i+1; j < getNumberOfCardsY() * getNumberOfCardsX(); j++) {
                if(cards[j].isFlipped() && !cards[j].hasMatch()){
                    cards[i].flip();
                    cards[j].flip();
                    changed =true;
                    break;
                }
            }
        }
    }
    
    if(changed){
        del->update();
    }
}

bool FindMatchGameController::isGameOver(){
    int count = 0;
    for (int i = 0; i < getNumberOfCardsY() * getNumberOfCardsX(); i++) {
        if(cards[i].hasMatch()){
            count++;
        }
    }
    
    return (count >= getNumberOfCardsY() * getNumberOfCardsX());
}