//
//  FindMatchGameController.h
//  SnapGame
//
//  Created by Sascha on 07.03.15.
//
//

#ifndef __SnapGame__FindMatchGameController__
#define __SnapGame__FindMatchGameController__

#include "MemoryCard.h"
#include "FindMatchGameLayer.h"

class FindMatchGameController{
private:
    static FindMatchGameController *instance;
    std::vector<MemoryCard> cards;
    int numberOfCardsX, numberOfCardsY;
    FindMatchGameLayer* del;
    bool locked;
    
public:
    static FindMatchGameController* getInstance();
    void initCards(int numberOfCardsX, int numberOfCardsY);
    MemoryCard getCard(int i);
    int getNumberOfCardsX();
    int getNumberOfCardsY();
    void setDelegate(FindMatchGameLayer* del);
    void tapCard(int i);
    void lock();
    void unlock();
    void finalizeRound();
    bool isGameOver();
    
protected:
    FindMatchGameController();
};

#endif /* defined(__SnapGame__FindMatchGameController__) */
