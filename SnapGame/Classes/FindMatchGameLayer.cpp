//
//  FindMatchGameLayer.cpp
//  SnapGame
//
//  Created by Sascha on 07.03.15.
//
//

#include "FindMatchGameLayer.h"
#include "FindMatchGameController.h"
#include "GameConstants.h"
#include "SceneController.h"

USING_NS_CC;

bool FindMatchGameLayer::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    
    initListener();
    refreshCards();
    
    height = GameConstants::CARD_TEXTURE_SIZE * FindMatchGameController::getInstance()->getNumberOfCardsY() + GameConstants::CARD_TEXTURE_SIZE * cardMargin * (FindMatchGameController::getInstance()->getNumberOfCardsY()-1);
    width =GameConstants::CARD_TEXTURE_SIZE * FindMatchGameController::getInstance()->getNumberOfCardsX() + GameConstants::CARD_TEXTURE_SIZE * cardMargin * (FindMatchGameController::getInstance()->getNumberOfCardsX()-1);
    
    FindMatchGameController::getInstance()->setDelegate(this);
    
    return true;
}

void FindMatchGameLayer::initListener(){
    touchListener = EventListenerTouchOneByOne::create();
    touchListener->setSwallowTouches(true);
    
    touchListener->onTouchBegan = [](Touch* touch, Event* event){
        auto target = static_cast<Sprite*>(event->getCurrentTarget());
        
        //Get the position of the current point relative to the button
        Point locationInNode = target->convertToNodeSpace(touch->getLocation());
        Size s = target->getContentSize();
        Rect rect = Rect(0, 0, s.width, s.height);
        log("sprite began... ");
        
        //Check the click area
        if (rect.containsPoint(locationInNode))
        {
            target->setOpacity(180);
            log("sprite began... x = %f, y = %f", locationInNode.x, locationInNode.y);
            
            return true;
        }
        return false;
    };
    
    touchListener->onTouchEnded = [=](Touch* touch, Event* event){
        auto target = static_cast<Sprite*>(event->getCurrentTarget());
        
        //Get the position of the current point relative to the button
        Point locationInNode = target->convertToNodeSpace(touch->getLocation());
        Size s = target->getContentSize();
        Rect rect = Rect(0, 0, s.width, s.height);
        log("sprite end... ");
        
        target->setOpacity(255);
        
        //Check the click area
        if (rect.containsPoint(locationInNode))
        {
            log("sprite number %s onTouchesEnded.. ",target->getName().c_str());
            
            int i = std::stoi(target->getName());
            
            auto tapCard = CallFunc::create([&]()
                                            {
                                                FindMatchGameController::getInstance()->tapCard(i);
                                            });
            
            auto finalizeRound = CallFunc::create([&]()
                                                  {
                                                      FindMatchGameController::getInstance()->finalizeRound();
                                                  });
            
            runAction(Sequence::create(tapCard, DelayTime::create(1.0f), finalizeRound, nullptr));
            
            
        }
    };
}

float FindMatchGameLayer::getHeight(){
    return height;
}

float FindMatchGameLayer::getWidth(){
    return width;
}

void FindMatchGameLayer::update(){
    
    initListener();
    refreshCards();
    
    if(FindMatchGameController::getInstance()->isGameOver()){
        SceneController::getInstance()->showGameOver(this);
    }
    
}

//sets either standard background or custom image as sprite texture
void FindMatchGameLayer::refreshCards(){
    this->removeAllChildren();
    
    for (int i = 0; i < FindMatchGameController::getInstance()->getNumberOfCardsY(); i++) {
        float y = i * GameConstants::CARD_TEXTURE_SIZE + (i * GameConstants::CARD_TEXTURE_SIZE * cardMargin) + GameConstants::CARD_TEXTURE_SIZE/2;
        
        for (int j = 0; j < FindMatchGameController::getInstance()->getNumberOfCardsX(); j++) {
            float x = j * GameConstants::CARD_TEXTURE_SIZE + (j * GameConstants::CARD_TEXTURE_SIZE * cardMargin) + GameConstants::CARD_TEXTURE_SIZE/2;
            if(!FindMatchGameController::getInstance()->getCard(i*FindMatchGameController::getInstance()->getNumberOfCardsX()+j).hasMatch()){
                
                Sprite* sp = Sprite::create();
                sp->setName(std::to_string(i*FindMatchGameController::getInstance()->getNumberOfCardsX()+j));
                sp->setTexture(FindMatchGameController::getInstance()->getCard(i*FindMatchGameController::getInstance()->getNumberOfCardsX()+j).getCustomImageFilePath());
                
                if(FindMatchGameController::getInstance()->getCard(i*FindMatchGameController::getInstance()->getNumberOfCardsX()+j).isFlipped()){
                    //add frame
                    Sprite* selection = Sprite::create("selectionRing_Blue.png");
                    selection->setPosition(GameConstants::CARD_TEXTURE_SIZE/2,GameConstants::CARD_TEXTURE_SIZE/2);
                    sp->addChild(selection,2);
                }
                
                sp->setPosition(x,y);
                
                //add frame
                Sprite* frame = Sprite::create("cardFrame.png");
                frame->setPosition(GameConstants::CARD_TEXTURE_SIZE/2,GameConstants::CARD_TEXTURE_SIZE/2);
                sp->addChild(frame,1);
                
                //add shadow
                Sprite* shadow = Sprite::create();
                shadow->setSpriteFrame(sp->getSpriteFrame());
                shadow->setScale(1.02);
                shadow->setColor(Color3B(0, 0, 0));
                shadow->setOpacity(60);
                shadow->setPosition(shadow->getContentSize().width/2, shadow->getContentSize().height/2);
                sp->addChild(shadow,-1);
                
                addChild(sp, 1);
                _eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener->clone(), sp);
            }
            
        }
    }
}