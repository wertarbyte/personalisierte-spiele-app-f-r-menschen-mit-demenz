//
//  FindMatchGameLayer.h
//  SnapGame
//
//  Created by Sascha on 07.03.15.
//
//

#ifndef __SnapGame__FindMatchGameLayer__
#define __SnapGame__FindMatchGameLayer__

#include "cocos2d.h"

class FindMatchGameLayer : public cocos2d::Layer{
private:
    float width, height;
    cocos2d::EventListenerTouchOneByOne* touchListener;
    float cardMargin = 0.2f;
    
    void initListener();
    void refreshCards();
    void finalize();
    
public:
    virtual bool init();
    void update();
    float getWidth();
    float getHeight();
    
    CREATE_FUNC(FindMatchGameLayer);
};

#endif /* defined(__SnapGame__FindMatchGameLayer__) */
