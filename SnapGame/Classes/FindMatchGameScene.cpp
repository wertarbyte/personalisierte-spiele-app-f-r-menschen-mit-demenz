//
//  FindMatchGameScene.cpp
//  SnapGame
//
//  Created by Sascha on 07.03.15.
//
//

#include "FindMatchGameScene.h"
#include "bg.h"
#include "FindMatchGameLayer.h"

bool FindMatchGameScene::init(){
    if(cocos2d::CCLayer::init()){
        
    }
    return true;
}

cocos2d::Scene* FindMatchGameScene::createScene(){
    cocos2d::Scene *scene = cocos2d::Scene::create();
    
    //add background
    cocos2d::Layer *backGroundLayer = bg::create();
    scene->addChild(backGroundLayer, 0);
    
    
    //add game layer
    FindMatchGameLayer* memoryGameLayer = FindMatchGameLayer::create();
    cocos2d::log("Bounding Box %f, %f", memoryGameLayer->getWidth(), memoryGameLayer->getHeight());
    
    float scaleFactor = 1.0f;
    
    //defines margin around gameLayer
    float gameLayerMargin = 0.8f;
    
    if(memoryGameLayer->getWidth() >= memoryGameLayer->getHeight() && memoryGameLayer->getWidth() >= cocos2d::Director::getInstance()->getWinSize().width * gameLayerMargin)
    {
        scaleFactor = (cocos2d::Director::getInstance()->getWinSize().width * gameLayerMargin / memoryGameLayer->getWidth());
    }else if(memoryGameLayer->getHeight() >= memoryGameLayer->getWidth() && memoryGameLayer->getWidth() && memoryGameLayer->getHeight() >= cocos2d::Director::getInstance()->getWinSize().height){
        scaleFactor =  (cocos2d::Director::getInstance()->getWinSize().height * gameLayerMargin / memoryGameLayer->getHeight());
    }
    
    memoryGameLayer->setScale(scaleFactor);
    memoryGameLayer->setPosition((cocos2d::Director::getInstance()->getWinSize().width/2 - memoryGameLayer->getWidth()/2) * scaleFactor, (cocos2d::Director::getInstance()->getWinSize().height/2 - memoryGameLayer->getHeight()/2) * scaleFactor);
    
    scene->addChild(memoryGameLayer, 1);
    
    return scene;
}