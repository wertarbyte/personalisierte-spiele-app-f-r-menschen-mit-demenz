//
//  FindMatchGameScene.h
//  SnapGame
//
//  Created by Sascha on 07.03.15.
//
//

#ifndef __SnapGame__FindMatchGameScene__
#define __SnapGame__FindMatchGameScene__

#include "cocos2d.h"

class FindMatchGameScene : public cocos2d::Layer{
public:
    virtual bool init();
    static cocos2d::Scene* createScene();
    
    CREATE_FUNC(FindMatchGameScene);
};

#endif /* defined(__SnapGame__FindMatchGameScene__) */
