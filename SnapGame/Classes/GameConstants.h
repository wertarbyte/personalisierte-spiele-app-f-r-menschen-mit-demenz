//
//  GameConstants.h
//  SnapGame
//
//  Created by Sascha Becker on 30.11.14.
//
//

#ifndef SnapGame_GameConstants_h
#define SnapGame_GameConstants_h

class GameConstants{
public:
    static const int CARD_TEXTURE_SIZE = 1024;
};

#endif
