#include "GameList.h"
#include "SceneController.h"

USING_NS_CC;

Scene* GameList::createScene()
{
    auto scene = Scene::create();
    auto layer = GameList::create();
    scene->addChild(layer);
    return scene;
}

bool GameList::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
   /* auto bg = Sprite::create("preview_background2.png");
    bg->setPosition(Point(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
    this->addChild(bg,-1);*/
    
    auto bg = Sprite::create("wood.png");
    auto s_visibleRect = Director::getInstance()->getOpenGLView()->getVisibleRect();
    float scaleY = s_visibleRect.size.height/bg->getContentSize().height;
    float scaleX = s_visibleRect.size.width/bg->getContentSize().width;
    if(scaleX>scaleY){
        bg->setScale(scaleX);
    }else{
        bg->setScale(scaleY);
    }
    bg->setPosition(Point(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
    this->addChild(bg,-1);
    
    
    auto backButton = MenuItemImage::create("buttonBackNormal.png", "buttonBackPressed.png", CC_CALLBACK_1(GameList::clickBack, this));
    auto gameMenu = Menu::create(backButton, NULL);
    gameMenu->setPosition(backButton->getContentSize().width/2, visibleSize.height - backButton->getContentSize().height/2);
    this->addChild(gameMenu,1);
    
    auto title = Sprite::create("title_chooseGame.png");
    auto titleMenuItem = MenuItemSprite::create(title, NULL, NULL);
    auto titleMenu = Menu::create(titleMenuItem, NULL);
    titleMenu->setPosition(visibleSize.width/2,visibleSize.height - title->getContentSize().height/2);
    this->addChild(titleMenu,1);
    
    auto memorySpriteNormal = Sprite::create("buttonMemoryNormal.png");
    auto memorySpritePressed = Sprite::create("buttonMemoryPressed.png");
    auto memoryMenuItem = MenuItemSprite::create(memorySpriteNormal, memorySpritePressed, CC_CALLBACK_1(GameList::clickMemory, this));
    
    auto dotMatrixSpriteNormal = Sprite::create("buttonDotMatrixNormal.png");
    auto dotMatrixSpritePressed = Sprite::create("buttonDotMatrixPressed.png");
    auto dotMatrixMenuItem = MenuItemSprite::create(dotMatrixSpriteNormal, dotMatrixSpritePressed, CC_CALLBACK_1(GameList::clickDotMatrix, this));
    
    auto speedMatchSpriteNormal = Sprite::create("buttonSpeedMatchNormal.png");
    auto speedMatchSpritePressed = Sprite::create("buttonSpeedMatchPressed.png");
    auto speedMatchMenuItem = MenuItemSprite::create(speedMatchSpriteNormal, speedMatchSpritePressed, CC_CALLBACK_1(GameList::clickSpeedMatch, this));
    
    auto finMatchSpriteNormal = Sprite::create("buttonFindMatchNormal.png");
    auto findMatchPressed = Sprite::create("buttonFindMatchPressed.png");
    auto findMatchMenuItem = MenuItemSprite::create(finMatchSpriteNormal, findMatchPressed, CC_CALLBACK_1(GameList::clickFindMatch, this));
    
    auto gameListMenu = Menu::create(memoryMenuItem, dotMatrixMenuItem, speedMatchMenuItem, findMatchMenuItem, NULL);
    gameListMenu->alignItemsVerticallyWithPadding(50);
    this->addChild(gameListMenu,1);
    
    return true;
}

void GameList::clickMemory(cocos2d::Ref* pSender){
    SceneController::getInstance()->showGameMemory(pSender);
}

void GameList::clickDotMatrix(cocos2d::Ref* pSender){
    SceneController::getInstance()->showGameDotMatrix(pSender);
}

void GameList::clickSpeedMatch(cocos2d::Ref* pSender){
    SceneController::getInstance()->showGameSpeedMatch(pSender);
}

void GameList::clickFindMatch(cocos2d::Ref* pSender){
    SceneController::getInstance()->showGameFindMatch(pSender);
}

void GameList::clickBack(cocos2d::Ref* pSender){
    SceneController::getInstance()->showMainMenu(pSender);
}