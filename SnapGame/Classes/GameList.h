#ifndef SnapGame_GameList_h
#define SnapGame_GameList_h

#include "cocos2d.h"

class GameList : public cocos2d::Layer
{
public:
    static cocos2d::Scene* createScene();
    virtual bool init();
private:
    void clickMemory(cocos2d::Ref* pSender);
    void clickDotMatrix(cocos2d::Ref* pSender);
    void clickSpeedMatch(cocos2d::Ref* pSender);
    void clickFindMatch(cocos2d::Ref* pSender);
    void clickBack(cocos2d::Ref* pSender);
    
    CREATE_FUNC(GameList);
    
};

#endif
