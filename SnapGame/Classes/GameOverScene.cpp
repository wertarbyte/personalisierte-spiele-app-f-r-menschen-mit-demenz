//
//  GameOverScene.cpp
//  SnapGame
//
//  Created by Sascha Becker on 05.12.14.
//
//

#include "GameOverScene.h"
#include "GameConstants.h"
#include "SceneController.h"

USING_NS_CC;

bool GameOverScene::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    auto backgroundSprite = Sprite::create("wood.png");
    
    auto s_visibleRect = Director::getInstance()->getOpenGLView()->getVisibleRect();
    float scaleY = s_visibleRect.size.height/backgroundSprite->getContentSize().height;
    float scaleX = s_visibleRect.size.width/backgroundSprite->getContentSize().width;
    if(scaleX>scaleY){
        backgroundSprite->setScale(scaleX);
    }else{
        backgroundSprite->setScale(scaleY);
    }
    backgroundSprite->setPosition(Point(s_visibleRect.origin.x+s_visibleRect.size.width/2, s_visibleRect.origin.y+s_visibleRect.size.height/2));
    this->addChild(backgroundSprite, -1);
    
    
    //add game over image
    float gameLayerMargin = 0.8f;
    float scaleFactor = 1.0f;
    
    Sprite* gameOverSprite = Sprite::create("gameOver.png");
    scaleFactor = cocos2d::Director::getInstance()->getWinSize().height * gameLayerMargin / GameConstants::CARD_TEXTURE_SIZE;
    
    gameOverSprite->setScale(scaleFactor);
    gameOverSprite->setPosition(cocos2d::Director::getInstance()->getWinSize().width/2,cocos2d::Director::getInstance()->getWinSize().height/2);
    
    this->addChild(gameOverSprite,0);
    
    auto restartSpriteNormal = Sprite::create("buttonRestartNormal.png");
    auto restartSpritePressed = Sprite::create("buttonRestartPressed.png");
    auto restartMenuItem = MenuItemSprite::create(restartSpriteNormal, restartSpritePressed, CC_CALLBACK_1(GameOverScene::clickRestart, this));
    
    auto mainMenuSpriteNormal = Sprite::create("buttonMainMenuNormal.png");
    auto mainMenuSpritePressed = Sprite::create("buttonMainMenuPressed.png");
    auto mainMenuMenuItem = MenuItemSprite::create(mainMenuSpriteNormal, mainMenuSpritePressed, CC_CALLBACK_1(GameOverScene::clickMainmenu, this));

    auto gameOverMenu = Menu::create(restartMenuItem, mainMenuMenuItem, NULL);
    gameOverMenu->alignItemsHorizontallyWithPadding(50);
    gameOverMenu->setPositionY(gameOverSprite->getPosition().y - gameOverSprite->getContentSize().height/2 - restartMenuItem->getContentSize().height - 10);
    this->addChild(gameOverMenu,2);
    
    return true;
}

cocos2d::Scene* GameOverScene::createScene(){
    auto scene = Scene::create();
    auto layer = GameOverScene::create();
    scene->addChild(layer);
    
    return scene;
}

void GameOverScene::clickRestart(cocos2d::Ref* pSender){
    SceneController::getInstance()->replay();
}

void GameOverScene::clickMainmenu(cocos2d::Ref* pSender){
    SceneController::getInstance()->showGameList(this);
}