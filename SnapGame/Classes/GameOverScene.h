//
//  GameOverScene.h
//  SnapGame
//
//  Created by Sascha Becker on 05.12.14.
//
//

#ifndef __SnapGame__GameOverScene__
#define __SnapGame__GameOverScene__

#include "cocos2d.h"

class GameOverScene : public cocos2d::Layer{
public:
    virtual bool init();
    static cocos2d::Scene* createScene();
    
    CREATE_FUNC(GameOverScene);
private:
    void clickRestart(cocos2d::Ref* pSender);
    void clickMainmenu(cocos2d::Ref* pSender);
    
};

#endif /* defined(__SnapGame__GameOverScene__) */
