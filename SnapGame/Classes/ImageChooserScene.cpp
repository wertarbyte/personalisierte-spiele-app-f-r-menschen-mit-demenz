//
//  ImageChooserScene.cpp
//  SnapGame
//
//  Created by Sascha Becker on 24.10.14.
//
//

#include "ImageChooserScene.h"
#include "SceneController.h"
#include "CustomImageManager.h"
#include "GameConstants.h"

USING_NS_CC;

Scene* ImageChooserScene::createScene()
{
    auto scene = Scene::create();
    auto layer = ImageChooserScene::create();
    scene->addChild(layer);
    return scene;
}

bool ImageChooserScene::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    bg = Sprite::create("wood.png");
    
    auto s_visibleRect = Director::getInstance()->getOpenGLView()->getVisibleRect();
    float scaleY = s_visibleRect.size.height/bg->getContentSize().height;
    float scaleX = s_visibleRect.size.width/bg->getContentSize().width;
    if(scaleX>scaleY){
        bg->setScale(scaleX);
    }else{
        bg->setScale(scaleY);
    }
    bg->setPosition(Point(s_visibleRect.origin.x+s_visibleRect.size.width/2, s_visibleRect.origin.y+s_visibleRect.size.height/2));
    this->addChild(bg, -1);
    
    auto backButton = MenuItemImage::create("buttonBackNormal.png", "buttonBackPressed.png", CC_CALLBACK_1(ImageChooserScene::back, this));
    auto gameMenu = Menu::create(backButton, NULL);
    gameMenu->setPosition(backButton->getContentSize().width/2, visibleSize.height - backButton->getContentSize().height/2);
    this->addChild(gameMenu,1);
    
    //init texfield for imagename
    int imageNameHeight = 100;
    imageName = TextFieldTTF::textFieldWithPlaceHolder("", cocos2d::Size(GameConstants::CARD_TEXTURE_SIZE,imageNameHeight), cocos2d::TextHAlignment::CENTER, "LinLibertine_R.ttf", 50);
    imageName->setPosition(visibleSize.width/2,visibleSize.height - imageNameHeight);
    imageName->setColor(cocos2d::Color3B::WHITE);
    //imageName->setOnEnterCallback([&]{ImageChooserScene::enterText();});
    this->addChild(imageName);
    
    //init picker
    picker = Sprite::create("cardBackground.jpg");
    picker->setPosition(visibleSize.width/2,visibleSize.height/2);
    this->addChild(picker);
    
    //init choose and save button
    auto addButton = MenuItemImage::create("buttonChooseImageNormal.png", "buttonChooseImagePressed.png", CC_CALLBACK_1(ImageChooserScene::chooseImage, this));
    auto editTextButton = MenuItemImage::create("buttonChangeNameNormal.png", "buttonChangeNamePressed.png", CC_CALLBACK_1(ImageChooserScene::enterText, this));
    auto saveButton = MenuItemImage::create("buttonSaveImageNormal.png", "buttonSaveImagePressed.png", CC_CALLBACK_1(ImageChooserScene::saveImage, this));
    auto addMenu = Menu::create(addButton, editTextButton, saveButton, NULL);
    addMenu->alignItemsHorizontallyWithPadding(10);

    addMenu->setPosition(origin.x + visibleSize.width/2, origin.y + addButton->getContentSize().height/2 + 10);
    this->addChild(addMenu);
    
    return true;
}

void ImageChooserScene::back(cocos2d::Ref* pSender){
    SceneController::getInstance()->showSettings(this);
}

void ImageChooserScene::chooseImage(Ref* pSender){
    del = new ImagePickerDelegateImpl(picker);
    ImagePicker::getInstance()->pickImage(del);
}

void ImageChooserScene::enterText(Ref* pSender){
    imageName->attachWithIME();
}

void ImageChooserScene::saveImage(Ref* pSender){
    picker->setName(imageName->getString());
    CustomImageManager::getInstance()->addCustomImageFromSprite(picker);
}

