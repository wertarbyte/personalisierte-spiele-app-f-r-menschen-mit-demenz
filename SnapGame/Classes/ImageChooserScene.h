//
//  ImageChooserScene.h
//  SnapGame
//
//  Created by Sascha Becker on 24.10.14.
//
//

#ifndef SnapGame_ImageChooserScene_h
#define SnapGame_ImageChooserScene_h

#include "cocos2d.h"
#include "ImagePickerDelegateImpl.h"

class ImageChooserScene : public cocos2d::Layer{
public:
    static cocos2d::Scene* createScene();
    virtual bool init();
    
    CREATE_FUNC(ImageChooserScene);
private:
    void back(Ref* pSender);
    void chooseImage(cocos2d::Ref* pSender);
    void saveImage(cocos2d::Ref* pSender);
    void showImage(cocos2d::Ref* pSender);
    void enterText(Ref* pSender);
    ImagePickerDelegateImpl* del;
    cocos2d::Sprite* picker;
    cocos2d::Sprite* bg;
    cocos2d::TextFieldTTF* imageName;
};


#endif
