//
//  ImagePickerDelegateImpl.cpp
//  SnapGame
//
//  Created by Sascha Becker on 22.10.14.
//
//

#include "ImagePickerDelegateImpl.h"
ImagePickerDelegateImpl::ImagePickerDelegateImpl(cocos2d::Sprite* _target){
    target = _target;
    
};

void ImagePickerDelegateImpl::didFinishPickingWithResult(cocos2d::Image* image){
    cocos2d::Texture2D* t = new cocos2d::Texture2D();
    t->initWithImage(image);
    
    target->setTexture(t);
    t->release();
    
    if(image->getWidth() <= image->getHeight())
        target->setTextureRect(cocos2d::Rect(0,image->getHeight()/2 - image->getWidth()/2,image->getWidth(),image->getWidth()));
    else
        target->setTextureRect(cocos2d::Rect(image->getWidth()/2 - image->getHeight()/2,0,image->getHeight(),image->getHeight()));
    
    float scalfator = 1024/target->getContentSize().width;
    target->setScale(scalfator);
    //target->setRotation(180);
};

