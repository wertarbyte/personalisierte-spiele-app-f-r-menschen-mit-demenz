//
//  ImagePickerDelegateImpl.h
//  SnapGame
//
//  Created by Sascha Becker on 22.10.14.
//
//

#ifndef SnapGame_ImagePickerDelegateImpl_h
#define SnapGame_ImagePickerDelegateImpl_h

#include "ImagePicker.h"

class ImagePickerDelegateImpl : public ImagePickerDelegate{
private:
    cocos2d::Sprite* target;
    unsigned char* data;
    ssize_t s;
public:
    ImagePickerDelegateImpl(cocos2d::Sprite* target);
    virtual void didFinishPickingWithResult(cocos2d::Image* result);
};

#endif
