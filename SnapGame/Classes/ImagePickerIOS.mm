#import <QuartzCore/QuartzCore.h>

#import "CCDirector.h"
#import "CCEAGLView-ios.h"

#import "ImagePicker.h"
#import "ImagePickerIOS.h"

@implementation ImagePickerIOS

-(void) takePicture
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    [imagePicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    [imagePicker setDelegate:self];
    imagePicker.wantsFullScreenLayout = YES;
    
    CCEAGLView *view = (CCEAGLView *)cocos2d::Director::getInstance()->getOpenGLView()->getEAGLView();
    [view addSubview:imagePicker.view];
}

-(void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    cocos2d::Image *imf =new cocos2d::Image();
    imf->autorelease();
    
    @autoreleasepool
    {
        NSData *imgData = UIImagePNGRepresentation(image);
        NSUInteger len = [imgData length];
        
        Byte *byteData = (Byte*)malloc(len);
        memcpy(byteData, [imgData bytes], len);
    
        imf->initWithImageData(byteData, len);
        free(byteData);
    }


    
    GLint m_maxTextureSize = 0;
    glGetIntegerv(GL_MAX_TEXTURE_SIZE, &m_maxTextureSize);
    
    if(imf->getHeight() > m_maxTextureSize || imf->getWidth() > m_maxTextureSize)
    {
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Fehler"
                                                    message:@"Das Bild ist leider zu groß!"
                                                    delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles:nil];
        [message show];
    }else{
        ImagePicker::getInstance()->finishImage(imf);
    }
    
    [picker.view removeFromSuperview];
    [picker release];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    ImagePicker::getInstance()->finishImage(nullptr);
    [picker.view removeFromSuperview];
    [picker release];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationLandscapeLeft);
}

@end