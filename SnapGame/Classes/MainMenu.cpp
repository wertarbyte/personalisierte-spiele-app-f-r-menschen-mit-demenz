#include "MainMenu.h"
#include "SceneController.h"
#include "DatabaseHelper.h"

USING_NS_CC;

Scene* MainMenu::createScene()
{
    auto scene = Scene::create();
    auto layer = MainMenu::create();
    scene->addChild(layer);
    return scene;
}

bool MainMenu::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    
    DatabaseHelper::getInstance();
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    auto bg = Sprite::create("wood.png");
    auto s_visibleRect = Director::getInstance()->getOpenGLView()->getVisibleRect();
    float scaleY = s_visibleRect.size.height/bg->getContentSize().height;
    float scaleX = s_visibleRect.size.width/bg->getContentSize().width;
    if(scaleX>scaleY){
        bg->setScale(scaleX);
    }else{
        bg->setScale(scaleY);
    }
    bg->setPosition(Point(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
    this->addChild(bg,-1);
    
    auto title = Sprite::create("mainTitle.png");
    auto titleMenuItem = MenuItemSprite::create(title, NULL, NULL);
    auto titleMenu = Menu::create(titleMenuItem, NULL);
    titleMenu->setPosition(visibleSize.width/2,visibleSize.height - title->getContentSize().height/2 - 100);
    this->addChild(titleMenu,1);
    
    auto playItem = MenuItemImage::create(
                                           "buttonPlayNormal.png",
                                           "buttonPlayPressed.png",
                                           CC_CALLBACK_1(MainMenu::showGameList, this));
    
    auto mainmenu = Menu::create(playItem, NULL);
    mainmenu->alignItemsVerticallyWithPadding(50);
    mainmenu->setPosition(Vec2(origin.x + visibleSize.width/2 ,
                               origin.y + visibleSize.height/2 - 200));
    this->addChild(mainmenu, 1);
    
    auto settingsButton = MenuItemImage::create("buttonSettingsNormal.png", "buttonSettingsPressed.png", CC_CALLBACK_1(MainMenu::showSettings, this));
    auto settingsMenu = Menu::create(settingsButton, NULL);
    settingsMenu->setPosition(Director::getInstance()->getWinSize().width - settingsButton->getContentSize().width/2, settingsButton->getContentSize().height/2);
    this->addChild(settingsMenu,1);
    
    return true;
}

void MainMenu::showGameList(cocos2d::Ref* pSender){
    SceneController::getInstance()->showGameList(pSender);
}

void MainMenu::showSettings(cocos2d::Ref* pSender){
    SceneController::getInstance()->showSettings(pSender);
    
}
