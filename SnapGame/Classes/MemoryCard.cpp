//
//  MemoryCard.cpp
//  SnapGame
//
//  Created by Sascha Becker on 06.11.14.
//
//

#include "MemoryCard.h"

MemoryCard::MemoryCard(CustomImage customImage){
    setCustomImage(customImage);
}

void MemoryCard::setCustomImage(CustomImage customImage){
    this->customImage = customImage;
}

std::string MemoryCard::getName(){
    return customImage.getName();
}

std::string MemoryCard::getCustomImageFilePath(){
    return (cocos2d::FileUtils::sharedFileUtils()->getWritablePath() + customImage.getPath());
}

void MemoryCard::flip(){
    cocos2d::log("flip");
    flipped = !flipped;
}

bool MemoryCard::isFlipped(){
    return flipped;
}

void MemoryCard::foundMatch(){
    matched = true;
}

bool MemoryCard::hasMatch(){
    return matched;
}
