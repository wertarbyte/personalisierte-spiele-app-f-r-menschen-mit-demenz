//
//  MemoryCard.h
//  SnapGame
//
//  Created by Sascha Becker on 06.11.14.
//
//

#ifndef SnapGame_MemoryCard_h
#define SnapGame_MemoryCard_h

#include "CustomImage.h"

class MemoryCard {
private:
    bool flipped;
    CustomImage customImage;
    bool matched;
    
public:
    MemoryCard(CustomImage customImage);
    void setCustomImage(CustomImage customImage);
    std::string getName();
    std::string getCustomImageFilePath();
    void flip();
    bool isFlipped();
    void foundMatch();
    bool hasMatch();
};

#endif
