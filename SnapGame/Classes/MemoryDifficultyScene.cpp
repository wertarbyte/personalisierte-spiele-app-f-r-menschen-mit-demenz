//
//  MemoryDifficultyScene.cpp
//  SnapGame
//
//  Created by Sascha Becker on 09.12.14.
//
//

#include "MemoryDifficultyScene.h"
#include "SceneController.h"
#include "CustomImagePool.h"
#include "bg.h"

USING_NS_CC;

Scene* MemoryDifficultyScene::createScene()
{
    auto scene = Scene::create();
    auto layer = MemoryDifficultyScene::create();
    scene->addChild(layer);
    return scene;
}

bool MemoryDifficultyScene::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    
    cocos2d::Layer *backGroundLayer = bg::create();
    this->addChild(backGroundLayer, -1);
    
    auto title = Sprite::create("title_chooseDifficulty.png");
    auto titleMenuItem = MenuItemSprite::create(title, NULL, NULL);
    auto titleMenu = Menu::create(titleMenuItem, NULL);
    titleMenu->setPosition(visibleSize.width/2,visibleSize.height - title->getContentSize().height/2);
    this->addChild(titleMenu,1);
    
    auto easySpriteNormal = Sprite::create("buttonEasyNormal.png");
    auto easySpritePressed = Sprite::create("buttonEasyPressed.png");
    auto easyMenuItem = MenuItemSprite::create(easySpriteNormal, easySpritePressed, CC_CALLBACK_1(MemoryDifficultyScene::clickEasy, this));
    
    auto moderateSpriteNormal = Sprite::create("buttonModerateNormal.png");
    auto moderateSpritePressed = Sprite::create("buttonModeratePressed.png");
    auto moderateMenuItem = MenuItemSprite::create(moderateSpriteNormal, moderateSpritePressed, CC_CALLBACK_1(MemoryDifficultyScene::clickModerate, this));
    
    auto hardSpriteNormal = Sprite::create("buttonHardNormal.png");
    auto hardSpritePressed = Sprite::create("buttonHardPressed.png");
    auto hardMenuItem = MenuItemSprite::create(hardSpriteNormal, hardSpritePressed, CC_CALLBACK_1(MemoryDifficultyScene::clickHard, this));
    
    auto gameListMenu = Menu::create(easyMenuItem, moderateMenuItem, hardMenuItem, NULL);
    gameListMenu->alignItemsVerticallyWithPadding(50);
    this->addChild(gameListMenu,1);
    
    return true;
}


void MemoryDifficultyScene::clickEasy(cocos2d::Ref* pSender){
    int x = 3, y = 2;
    if(CustomImagePool::getInstance()->getNumberOfCustomImages() >= x*y/2)
        SceneController::getInstance()->startMemory(x,y);
}

void MemoryDifficultyScene::clickModerate(cocos2d::Ref* pSender){
    int x = 4, y = 3;
    if(CustomImagePool::getInstance()->getNumberOfCustomImages() >= x*y/2)
        SceneController::getInstance()->startMemory(x,y);
}

void MemoryDifficultyScene::clickHard(cocos2d::Ref* pSender){
    int x = 5, y = 4;
    if(CustomImagePool::getInstance()->getNumberOfCustomImages() >= x*y/2)
        SceneController::getInstance()->startMemory(x,y);
}