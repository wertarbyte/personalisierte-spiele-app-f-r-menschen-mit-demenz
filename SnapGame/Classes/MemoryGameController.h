#ifndef __SnapGame__MemoryGameController__
#define __SnapGame__MemoryGameController__

#include "MemoryCard.h"
#include "MemoryGameLayer.h"

class MemoryGameController{
private:
    static MemoryGameController *instance;
    std::vector<MemoryCard> cards;
    int numberOfCardsX, numberOfCardsY;
    MemoryGameLayer* del;
    bool locked;
    
public:
    static MemoryGameController* getInstance();
    void initCards(int numberOfCardsX, int numberOfCardsY);
    MemoryCard getCard(int i);
    int getNumberOfCardsX();
    int getNumberOfCardsY();
    void setDelegate(MemoryGameLayer* del);
    void tapCard(int i);
    void lock();
    void unlock();
    void finalizeRound();
    bool isGameOver();
    
protected:
    MemoryGameController();
};

#endif /* defined(__SnapGame__MemoryGameController__) */
