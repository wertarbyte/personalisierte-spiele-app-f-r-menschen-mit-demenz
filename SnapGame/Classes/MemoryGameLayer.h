//
//  MemoryGameLayer.h
//  SnapGame
//
//  Created by Sascha Becker on 03.12.14.
//
//

#ifndef __SnapGame__MemoryGameLayer__
#define __SnapGame__MemoryGameLayer__

#include "cocos2d.h"

class MemoryGameLayer : public cocos2d::Layer{
private:
    float width, height;
    cocos2d::EventListenerTouchOneByOne* touchListener;
    float cardMargin = 0.2f;
    
    void initListener();
    void refreshCards();
    void finalize();
    
public:
    virtual bool init();
    void update();
    float getWidth();
    float getHeight();
    
    CREATE_FUNC(MemoryGameLayer);
};

#endif /* defined(__SnapGame__MemoryGameLayer__) */
