//
//  test.h
//  SnapGame
//
//  Created by Sascha Becker on 29.11.14.
//
//

#ifndef __SnapGame__test__
#define __SnapGame__test__

#include "cocos2d.h"

class MemoryGameScene : public cocos2d::Layer{
public:
    virtual bool init();    
    static cocos2d::Scene* createScene();
    
    CREATE_FUNC(MemoryGameScene);
};
#endif /* defined(__SnapGame__test__) */
