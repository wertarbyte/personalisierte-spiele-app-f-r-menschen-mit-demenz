#include "SceneController.h"
#include "MainMenu.h"
#include "GameList.h"
#include "SettingsScene.h"
#include "ImageChooserScene.h"
#include "CustomImageGalleryScene.h"
#include "MemoryGameScene.h"
#include "MemoryGameController.h"
#include "MemoryDifficultyScene.h"
#include "DotMatrixGameScene.h"
#include "DotMatrixGameController.h"
#include "DotMatrixDifficultyScene.h"
#include "SpeedMatchGameScene.h"
#include "SpeedMatchScene.h"
#include "SpeedMatchDifficultyScene.h"
#include "SpeedMatchGameController.h"
#include "FindMatchGameScene.h"
#include "FindMatchDifficultyScene.h"
#include "FindMatchGameController.h"
#include "GameOverScene.h"
#include "ExplanationScene.h"
#include "AboutScene.h"

USING_NS_CC;

int game = -1;
int value1 = -1;
int value2 = -1;
int value3 = -1;

SceneController* SceneController::instanz = 0;

SceneController* SceneController::getInstance(){
    if(instanz == 0)
        instanz = new SceneController();
    return instanz;
}

void SceneController::showScene(Scene* scene){
    auto director = Director::getInstance();
    director->replaceScene(TransitionCrossFade::create(0.1, scene));
}

void SceneController::showMainMenu(Ref* pSender){
    showScene(MainMenu::createScene());
}

void SceneController::showGameList(Ref* pSender){
    showScene(GameList::createScene());
}

void SceneController::showSettings(Ref* pSender){
    showScene(SettingsScene::createScene());
}

void SceneController::showImageChooser(Ref* pSender){
    showScene(ImageChooserScene::createScene());
}

void SceneController::showCustomImageGallery(Ref* pSender){
    showScene(CustomImageGalleryScene::createScene());
}

void SceneController::showGameMemory(Ref* pSender){
    showScene(MemoryDifficultyScene::createScene());
}

void SceneController::showGameDotMatrix(Ref* pSender){
    showScene(DotMatrixDifficultyScene::createScene());
}

void SceneController::showGameSpeedMatch(Ref* pSender){
    showScene(SpeedMatchDifficultyScene::createScene());
}

void SceneController::showGameFindMatch(Ref* pSender){
    showScene(FindMatchDifficultyScene::createScene());
}

void SceneController::showGameOver(Ref* pSender){
    showScene(GameOverScene::createScene());
}

void SceneController::showExplanation(Ref* pSender){
    showScene(ExplananationScene::createScene());
}

void SceneController::showAbout(Ref* pSender){
    showScene(AboutScene::createScene());
}

void SceneController::startMemory(int x, int y){
    value1 = x;
    value2 = y;
    game = 0;
    
    MemoryGameController::getInstance()->initCards(x, y);
    showScene(MemoryGameScene::createScene());
}

void SceneController::startDotMatrix(int dots, int glow, int options){
    value1 = dots;
    value2 = glow;
    value3 = options;
    game = 1;
    
    DotMatrixGameController::getInstance()->initDots(dots, glow, options);
    showScene(DotMatrixGameScene::createScene());
}

void SceneController::startSpeedMatch(int cardsToMemorize, int rounds){
    value1 = cardsToMemorize;
    value2 = rounds;
    game = 2;
    
    SpeedMatchGameController::getInstance()->initMemorizeCards(cardsToMemorize, rounds);
    showScene(SpeedMatchGameScene::createScene());
}

void SceneController::startFindMatch(int x, int y){
    value1 = x;
    value2 = y;
    game = 3;
    
    FindMatchGameController::getInstance()->initCards(x, y);
    showScene(FindMatchGameScene::createScene());
}

void SceneController::replay(){
    if(game == 0){
        MemoryGameController::getInstance()->initCards(value1, value2);
        showScene(MemoryGameScene::createScene());
    }else if(game == 1){
        DotMatrixGameController::getInstance()->initDots(value1, value2, value3);
        showScene(DotMatrixGameScene::createScene());
    }else if(game == 2){
        SpeedMatchGameController::getInstance()->initMemorizeCards(value1, value2);
        showScene(SpeedMatchGameScene::createScene());
    }else if(game == 3){
        FindMatchGameController::getInstance()->initCards(value1, value2);
        showScene(FindMatchGameScene::createScene());
    }
}
