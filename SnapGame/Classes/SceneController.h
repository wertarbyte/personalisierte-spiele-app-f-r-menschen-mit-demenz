#ifndef SnapGame_SceneController_h
#define SnapGame_SceneController_h

#include "cocos2d.h"

class SceneController
{
public:
    static SceneController* getInstance();
    
    void showMainMenu(cocos2d::Ref* pSender);
    void showSettings(cocos2d::Ref* pSender);
    void showGameList(cocos2d::Ref* pSender);
    void showImageChooser(cocos2d::Ref* pSender);
    void showGameMemory(cocos2d::Ref* pSender);
    void showGameDotMatrix(cocos2d::Ref* pSender);
    void showGameSpeedMatch(cocos2d::Ref* pSender);
    void showGameFindMatch(cocos2d::Ref* pSender);
    void showCustomImageGallery(cocos2d::Ref* pSender);
    void showGameOver(cocos2d::Ref* pSender);
    void showExplanation(cocos2d::Ref* pSender);
    void showAbout(cocos2d::Ref* pSender);
    
    //diffculty choices
    void startMemory(int x, int y);
    void startDotMatrix(int dots, int glow, int options);
    void startSpeedMatch(int cardsToMemorize, int rounds);
    void startFindMatch(int x, int y);
    
    //restart last played game
    void replay();
    
protected:
    SceneController(){}
    
private:
    static SceneController* instanz;
    void showScene(cocos2d::Scene* scene);
    
};

#endif
