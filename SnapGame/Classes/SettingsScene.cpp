#include "SettingsScene.h"
#include "SceneController.h"

USING_NS_CC;

Scene* SettingsScene::createScene()
{
    auto scene = Scene::create();
    auto layer = SettingsScene::create();
    scene->addChild(layer);
    return scene;
}

bool SettingsScene::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    auto bg = Sprite::create("wood.png");
    
    auto s_visibleRect = Director::getInstance()->getOpenGLView()->getVisibleRect();
    float scaleY = s_visibleRect.size.height/bg->getContentSize().height;
    float scaleX = s_visibleRect.size.width/bg->getContentSize().width;
    if(scaleX>scaleY){
        bg->setScale(scaleX);
    }else{
        bg->setScale(scaleY);
    }
    bg->setPosition(Point(s_visibleRect.origin.x+s_visibleRect.size.width/2, s_visibleRect.origin.y+s_visibleRect.size.height/2));
    this->addChild(bg, -1);
    
    auto backButton = MenuItemImage::create("buttonBackNormal.png", "buttonBackPressed.png", CC_CALLBACK_1(SettingsScene::back, this));
    auto gameMenu = Menu::create(backButton, NULL);
    gameMenu->setPosition(backButton->getContentSize().width/2, visibleSize.height - backButton->getContentSize().height/2);
    this->addChild(gameMenu,1);
    
    auto showAddNewImageItem = MenuItemImage::create("buttonAddImageNormal.png", "buttonAddImagePressed.png", CC_CALLBACK_1(SettingsScene::showImageChooser, this));
    auto showAllImagesItem = MenuItemImage::create("buttonShowImagesNormal.png", "buttonShowImagesPressed.png", CC_CALLBACK_1(SettingsScene::showCustomImageGallery, this));
    auto showExplanationItem = MenuItemImage::create("buttonExplanationNormal.png", "buttonExplanationPressed.png", CC_CALLBACK_1(SettingsScene::showExplanation, this));
    auto showAboutItem = MenuItemImage::create("buttonAboutNormal.png", "buttonAboutNormal.png", CC_CALLBACK_1(SettingsScene::showAbout, this));
    
    auto imageMenu = Menu::create(showAddNewImageItem, showAllImagesItem, showExplanationItem,showAboutItem,  NULL);
    //imageMenu->setPosition(Vec2(origin.x + visibleSize.width/2, origin.y + visibleSize.height - addImageButtonItem->getContentSize().height - 5));
    imageMenu->alignItemsVerticallyWithPadding(50);
    this->addChild(imageMenu, 1);
    
    return true;
}

void SettingsScene::back(Ref* pSender){
    SceneController::getInstance()->showMainMenu(pSender);
}

void SettingsScene::showImageChooser(Ref* pSender){
    SceneController::getInstance()->showImageChooser(pSender);
}

void SettingsScene::showCustomImageGallery(Ref* pSender){
    SceneController::getInstance()->showCustomImageGallery(pSender);
}

void SettingsScene::showExplanation(Ref* pSender){
    SceneController::getInstance()->showExplanation(pSender);
}

void SettingsScene::showAbout(Ref* pSender){
    SceneController::getInstance()->showAbout(pSender);
}
