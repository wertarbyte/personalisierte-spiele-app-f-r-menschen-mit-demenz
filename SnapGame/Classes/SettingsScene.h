#ifndef __SETTINGS_SCENE_H__
#define __SETTINGS_SCENE_H__

#include "cocos2d.h"
#include "ImagePickerDelegateImpl.h"

class SettingsScene : public cocos2d::Layer
{
private:
    cocos2d::Texture2D* image;
    void showImageChooser(Ref* pSender);
    void showCustomImageGallery(Ref* pSender);
    void showExplanation(Ref* pSender);
    void showAbout(Ref* pSender);
    void back(Ref* pSender);
    cocos2d::Sprite* display;
public:
    static cocos2d::Scene* createScene();
    virtual bool init();  
    CREATE_FUNC(SettingsScene);
};

#endif
