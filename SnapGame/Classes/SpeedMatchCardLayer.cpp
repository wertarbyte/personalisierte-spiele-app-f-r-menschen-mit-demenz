//
//  SpeedMatchCardLayer.cpp
//  SnapGame
//
//  Created by Sascha Becker on 09.12.14.
//
//

#include "SpeedMatchCardLayer.h"
#include "SpeedMatchGameController.h"
#include "GameConstants.h"

USING_NS_CC;

bool SpeedMatchCardLayer::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    
    width = GameConstants::CARD_TEXTURE_SIZE * SpeedMatchGameController::getInstance()->getCardsToMemorize() + GameConstants::CARD_TEXTURE_SIZE * cardMargin * (SpeedMatchGameController::getInstance()->getCardsToMemorize()-1);
    height = GameConstants::CARD_TEXTURE_SIZE;
    
    SpeedMatchGameController::getInstance()->setCardDelegate(this);
    
    initListener();
    
    //init sprites
    for (int i = 0; i < SpeedMatchGameController::getInstance()->getCardsToMemorize(); i++) {
        //init cards
        Sprite* sp = Sprite::create();
        _eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener->clone(), sp);
        
        sp->setName(std::to_string(i));
        
        //add frame to card
        Sprite* frame = Sprite::create("cardFrame.png");
        frame->setPosition(GameConstants::CARD_TEXTURE_SIZE/2,GameConstants::CARD_TEXTURE_SIZE/2);
        sp->addChild(frame);
        addChild(sp,1);
        cards.pushBack(sp);

        //init correctORwrong
        Sprite* sp2 = Sprite::create();
        sp2->setName(std::to_string(i));
        sp2->setTexture("checkmark.png");
        
        addChild(sp2,2);
        correctORwrong.pushBack(sp2);
        
        //fade them out to make them invisile at start
        //Action* action = Sequence::create(FadeOut::create(0.5), NULL);
        //correctORwrong.at(i)->runAction(action);
    }
    
    refreshCards();
    
    return true;
}

void SpeedMatchCardLayer::initListener(){
    touchListener = EventListenerTouchOneByOne::create();
    touchListener->setSwallowTouches(true);
    
    touchListener->onTouchBegan = [](Touch* touch, Event* event){
        auto target = static_cast<Sprite*>(event->getCurrentTarget());
        
        //Get the position of the current point relative to the button
        Point locationInNode = target->convertToNodeSpace(touch->getLocation());
        Size s = target->getContentSize();
        Rect rect = Rect(0, 0, s.width, s.height);
        log("sprite began... ");
        
        //Check the click area
        if (rect.containsPoint(locationInNode))
        {
            target->setOpacity(220);
            log("sprite began... x = %f, y = %f", locationInNode.x, locationInNode.y);
            
            return true;
        }
        return false;
    };
    
    touchListener->onTouchEnded = [=](Touch* touch, Event* event){
        auto target = static_cast<Sprite*>(event->getCurrentTarget());
        
        //Get the position of the current point relative to the button
        Point locationInNode = target->convertToNodeSpace(touch->getLocation());
        Size s = target->getContentSize();
        Rect rect = Rect(0, 0, s.width, s.height);
        log("sprite end... ");
        
        target->setOpacity(255);
        
        //Check the click area
        if (rect.containsPoint(locationInNode))
        {
            log("sprite number %s onTouchesEnded.. ",target->getName().c_str());
            
            //only if in comparemode
            if(SpeedMatchGameController::getInstance()->getState() == 1){
                int i = std::stoi(target->getName());
                
                bool answer = SpeedMatchGameController::getInstance()->isCorrect(i);
                
                if(answer){
                    correctORwrong.at(i)->setTexture("checkmark.png");
                }else{
                    correctORwrong.at(i)->setTexture("cross.png");
                }
                
                Action* action = Sequence::create(FadeIn::create(0.5),FadeOut::create(0.3), NULL);
                correctORwrong.at(i)->runAction(action);
            }
        }
    };
}

float SpeedMatchCardLayer::getHeight(){
    return height;
}

float SpeedMatchCardLayer::getWidth(){
    return width;
}

void SpeedMatchCardLayer::update(){
    initListener();
    animateAndRefreshCards();
}

void SpeedMatchCardLayer::animateAndRefreshCards(){
    stopAllActions();
    for (int i = 0; i < cards.size(); i++) {
        Action* action = Sequence::create(FadeOut::create(0.01), NULL);
        correctORwrong.at(i)->runAction(action);
    }
    
    auto refresh = CallFunc::create([&]()
                                    {
                                        refreshCards();
                                    });
    
    for (int i = 0; i < cards.size(); i++) {
        Vec2 origin = cards.at(i)->getPosition();
        Action* action2;
        if(i == cards.size()-1){ // if last card, add refreshCards
            action2 = Sequence::create(MoveBy::create(0.3f, Vec2(0, Director::getInstance()->getWinSize().height) * (1 + (1 - getScale()))), MoveTo::create(0.01f, origin), refresh, NULL);
        }else{
            action2 = Sequence::create(MoveBy::create(0.3f, Vec2(0, Director::getInstance()->getWinSize().height) * (1 + (1 - getScale()))), MoveTo::create(0.01f, origin), NULL);
        }
        cards.at(i)->runAction(action2);
    }
}

void SpeedMatchCardLayer::refreshCards(){
    //stopAllActions();
    for (int i = 0; i < cards.size(); i++) {
        float x = i * GameConstants::CARD_TEXTURE_SIZE + (i * GameConstants::CARD_TEXTURE_SIZE * cardMargin) + GameConstants::CARD_TEXTURE_SIZE/2;
        
        //set texture and position for cards
        cards.at(i)->setTexture(SpeedMatchGameController::getInstance()->getCardImagePath(i));
        cards.at(i)->setPosition(x , GameConstants::CARD_TEXTURE_SIZE/2);
        
        correctORwrong.at(i)->setPosition(x, GameConstants::CARD_TEXTURE_SIZE/2);
    }
}