//
//  SpeedMatchCardLayer.h
//  SnapGame
//
//  Created by Sascha Becker on 09.12.14.
//
//

#ifndef __SnapGame__SpeedMatchCardLayer__
#define __SnapGame__SpeedMatchCardLayer__

#include "cocos2d.h"

class SpeedMatchCardLayer : public cocos2d::Layer{
private:
    float width, height;
    cocos2d::EventListenerTouchOneByOne* touchListener;
    float cardMargin = 0.2f;
    cocos2d::Vector<cocos2d::Sprite*> cards;
    cocos2d::Vector<cocos2d::Sprite*> correctORwrong;
    
    void initListener();
    void refreshCards();
    void animateAndRefreshCards();
    
public:
    virtual bool init();
    void update();
    float getWidth();
    float getHeight();
    
    CREATE_FUNC(SpeedMatchCardLayer);
};

#endif /* defined(__SnapGame__SpeedMatchCardLayer__) */
