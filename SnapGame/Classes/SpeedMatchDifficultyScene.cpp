//
//  SpeedMatchDifficultyScene.cpp
//  SnapGame
//
//  Created by Sascha Becker on 09.12.14.
//
//

#include "SpeedMatchDifficultyScene.h"
#include "SceneController.h"
#include "bg.h"

USING_NS_CC;

Scene* SpeedMatchDifficultyScene::createScene()
{
    auto scene = Scene::create();
    auto layer = SpeedMatchDifficultyScene::create();
    scene->addChild(layer);
    return scene;
}

bool SpeedMatchDifficultyScene::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    
    cocos2d::Layer *backGroundLayer = bg::create();
    this->addChild(backGroundLayer, -1);
    
    auto title = Sprite::create("title_chooseDifficulty.png");
    auto titleMenuItem = MenuItemSprite::create(title, NULL, NULL);
    auto titleMenu = Menu::create(titleMenuItem, NULL);
    titleMenu->setPosition(visibleSize.width/2,visibleSize.height - title->getContentSize().height/2);
    this->addChild(titleMenu,1);
    
    auto easySpriteNormal = Sprite::create("buttonEasyNormal.png");
    auto easySpritePressed = Sprite::create("buttonEasyPressed.png");
    auto easyMenuItem = MenuItemSprite::create(easySpriteNormal, easySpritePressed, CC_CALLBACK_1(SpeedMatchDifficultyScene::clickEasy, this));
    
    auto moderateSpriteNormal = Sprite::create("buttonModerateNormal.png");
    auto moderateSpritePressed = Sprite::create("buttonModeratePressed.png");
    auto moderateMenuItem = MenuItemSprite::create(moderateSpriteNormal, moderateSpritePressed, CC_CALLBACK_1(SpeedMatchDifficultyScene::clickModerate, this));
    
    auto hardSpriteNormal = Sprite::create("buttonHardNormal.png");
    auto hardSpritePressed = Sprite::create("buttonHardPressed.png");
    auto hardMenuItem = MenuItemSprite::create(hardSpriteNormal, hardSpritePressed, CC_CALLBACK_1(SpeedMatchDifficultyScene::clickHard, this));
    
    auto gameListMenu = Menu::create(easyMenuItem, moderateMenuItem, hardMenuItem, NULL);
    gameListMenu->alignItemsVerticallyWithPadding(50);
    this->addChild(gameListMenu,1);
    
    return true;
}


void SpeedMatchDifficultyScene::clickEasy(cocos2d::Ref* pSender){
    SceneController::getInstance()->startSpeedMatch(2,10);
}

void SpeedMatchDifficultyScene::clickModerate(cocos2d::Ref* pSender){
    SceneController::getInstance()->startSpeedMatch(3,12);
}

void SpeedMatchDifficultyScene::clickHard(cocos2d::Ref* pSender){
    SceneController::getInstance()->startSpeedMatch(4,15);
}