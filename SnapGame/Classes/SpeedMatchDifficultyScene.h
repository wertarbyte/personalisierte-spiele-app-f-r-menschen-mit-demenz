//
//  SpeedMatchDifficultyScene.h
//  SnapGame
//
//  Created by Sascha Becker on 09.12.14.
//
//

#ifndef __SnapGame__SpeedMatchDifficultyScene__
#define __SnapGame__SpeedMatchDifficultyScene__

#include "cocos2d.h"

class SpeedMatchDifficultyScene : public cocos2d::Layer{
public:
    static cocos2d::Scene* createScene();
    virtual bool init();
private:
    void clickEasy(cocos2d::Ref* pSender);
    void clickModerate(cocos2d::Ref* pSender);
    void clickHard(cocos2d::Ref* pSender);
    
    CREATE_FUNC(SpeedMatchDifficultyScene);
};

#endif /* defined(__SnapGame__SpeedMatchDifficultyScene__) */
