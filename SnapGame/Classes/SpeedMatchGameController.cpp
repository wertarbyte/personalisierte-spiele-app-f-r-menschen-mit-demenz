//
//  SpeedMatchGameController.cpp
//  SnapGame
//
//  Created by Sascha Becker on 05.12.14.
//
//

#include "SpeedMatchGameController.h"
#include "CustomImagePool.h"
#include "SceneController.h"

SpeedMatchGameController* SpeedMatchGameController::instance = 0;

SpeedMatchGameController::SpeedMatchGameController(){
    
}

SpeedMatchGameController* SpeedMatchGameController::getInstance(){
    if(instance == 0)
        instance = new SpeedMatchGameController();
    return instance;
}

void SpeedMatchGameController::initMemorizeCards(int cardsToMemorize, int rounds){
    memorizeCards.clear();
    compareCardPool.clear();
    state = 0;
    currentRound = 0;
    countCorrect = 0;
    countWrong = 0;
    
    this->cardsToMemorize = cardsToMemorize;
    this->rounds = rounds;
    
    memorizeCards = CustomImagePool::getInstance()->getRandomCustomImages(cardsToMemorize);
    
    // get 10 random cards for comparision
    compareCardPool = CustomImagePool::getInstance()->getRandomCustomImages(10);
    // include remember cards
    for (int j = 0; j < cardsToMemorize; j++) {
        compareCardPool.push_back(memorizeCards.at(j));
    }
    
    refreshCompareCards();
}

void SpeedMatchGameController::refreshCompareCards(){
    //compareCards = CustomImagePool::getInstance()->getRandomCustomImages(cardsToMemorize);
    compareCards.clear();
    
        while (compareCards.size() < cardsToMemorize) {
            CustomImage ci;
            
            int rand = cocos2d::rand_0_1() * compareCardPool.size();
            ci = compareCardPool[rand];
            
            bool contains = false;
            
            for (int i=0; i < compareCards.size(); i++) {
                if(ci.equals(compareCards[i])){
                    contains = true;
                }
            }
            
            if(!contains){
                compareCards.push_back(ci);
            }
        }
    
}

std::string SpeedMatchGameController::getCardImagePath(int i){
    if(state == 0){ // memorize state
        return cocos2d::FileUtils::sharedFileUtils()->getWritablePath() + memorizeCards[i].getPath();
    }else{ // compare state
        return cocos2d::FileUtils::sharedFileUtils()->getWritablePath() + compareCards[i].getPath();
    }
}

int SpeedMatchGameController::getState(){
    return state;
}

int SpeedMatchGameController::getRounds(){
    return rounds;
}

int SpeedMatchGameController::getCurrentRound(){
    return currentRound;
}

int SpeedMatchGameController::getCardsToMemorize(){
    return cardsToMemorize;
}

int SpeedMatchGameController::getCountCorrect(){
    return countCorrect;
}

int SpeedMatchGameController::getCountWrong(){
    return countWrong;
}

void SpeedMatchGameController::setCardDelegate(SpeedMatchCardLayer* del){
    this->cardDel = del;
}

void SpeedMatchGameController::setStatDelegate(SpeedMatchStatLayer* del){
    this->statDel = del;
}

void SpeedMatchGameController::setUIDelegate(SpeedMatchUILayer* del){
    this->UIDel = del;
}

void SpeedMatchGameController::tapGo(){
    if(state == 0){
        state = 1; // switch state from remember to compare mode
        UIDel->update();
        cardDel->update();
    }else{ // increment round for counter till game finished
        currentRound++;
        refreshCompareCards();
        cardDel->update();
    }
    
    statDel->update();
    
    if(currentRound == rounds){
        instance = 0;
        SceneController::getInstance()->showGameOver(nullptr);
    }
}

bool SpeedMatchGameController::isCorrect(int i){
    bool answer = false;
    
    for (int j = 0; j < cardsToMemorize; j++) {
        if(memorizeCards.at(j).getName() == compareCards.at(i).getName())
        {
            answer = true;
            break;
        }
    }
    
    if(answer)
        countCorrect++;
    else
        countWrong++;
    
    statDel->update();
    
    return answer;
}