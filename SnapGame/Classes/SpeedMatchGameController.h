//
//  SpeedMatchGameController.h
//  SnapGame
//
//  Created by Sascha Becker on 05.12.14.
//
//

#ifndef __SnapGame__SpeedMatchGameController__
#define __SnapGame__SpeedMatchGameController__

#include "CustomImage.h"
#include "SpeedMatchCardLayer.h"
#include "SpeedMatchStatLayer.h"
#include "SpeedMatchUILayer.h"

class SpeedMatchGameController{
private:
    static SpeedMatchGameController *instance;
    
    //delegates
    SpeedMatchCardLayer* cardDel;
    SpeedMatchStatLayer* statDel;
    SpeedMatchUILayer* UIDel;
    
    void refreshCompareCards();
    
    int state = 0; // 0 = memorize cards, 1 = compare cards
    std::vector<CustomImage> memorizeCards;
    std::vector<CustomImage> compareCardPool;
    std::vector<CustomImage> compareCards;
    int rounds;
    int currentRound = 0;
    int cardsToMemorize;
    int countCorrect = 0;
    int countWrong = 0;
    
public:
    static SpeedMatchGameController* getInstance();
    void initMemorizeCards(int cardsToMemorize, int rounds);
    void tapGo();
    void tapCard();
    int getState();
    int getCurrentRound();
    int getRounds();
    int getCardsToMemorize();
    int getCountCorrect();
    int getCountWrong();
    std::string getCardImagePath(int i);
    bool isCorrect(int i);
    
    void setCardDelegate(SpeedMatchCardLayer* del);
    void setStatDelegate(SpeedMatchStatLayer* del);
    void setUIDelegate(SpeedMatchUILayer* del);
    
protected:
    SpeedMatchGameController();
};

#endif /* defined(__SnapGame__SpeedMatchGameController__) */
