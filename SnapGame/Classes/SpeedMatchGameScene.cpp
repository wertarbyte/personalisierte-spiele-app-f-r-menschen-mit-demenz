//
//  SpeedMatchGameScene.cpp
//  SnapGame
//
//  Created by Sascha Becker on 06.12.14.
//
//

#include "SpeedMatchGameScene.h"
#include "SpeedMatchUILayer.h"
#include "SpeedMatchCardLayer.h"
#include "SpeedMatchStatLayer.h"
#include "bg.h"

bool SpeedMatchGameScene::init(){
    if(cocos2d::CCLayer::init()){
        
    }
    return true;
}

cocos2d::Scene* SpeedMatchGameScene::createScene(){
    cocos2d::Scene *scene = cocos2d::Scene::create();
    
    //add background
    cocos2d::Layer *backGroundLayer = bg::create();
    scene->addChild(backGroundLayer, -1);
    
    //add game layer
    SpeedMatchCardLayer* speedMatchCardLayer = SpeedMatchCardLayer::create();
    cocos2d::log("Bounding Box %f, %f", speedMatchCardLayer->getWidth(), speedMatchCardLayer->getHeight());
    
    float scaleFactor = 1.0f;
    
    //defines margin around gameLayer
    float gameLayerMargin = 0.95f;
    
    if(speedMatchCardLayer->getWidth() >= speedMatchCardLayer->getHeight() && speedMatchCardLayer->getWidth() >= cocos2d::Director::getInstance()->getWinSize().width * gameLayerMargin)
    {
        scaleFactor = (cocos2d::Director::getInstance()->getWinSize().width * gameLayerMargin / speedMatchCardLayer->getWidth());
    }else if(speedMatchCardLayer->getHeight() >= speedMatchCardLayer->getWidth() && speedMatchCardLayer->getWidth() && speedMatchCardLayer->getHeight() >= cocos2d::Director::getInstance()->getWinSize().height){
        scaleFactor =  (cocos2d::Director::getInstance()->getWinSize().height * gameLayerMargin / speedMatchCardLayer->getHeight());
    }
    
    speedMatchCardLayer->setScale(scaleFactor);
    speedMatchCardLayer->setPosition((cocos2d::Director::getInstance()->getWinSize().width/2 - speedMatchCardLayer->getWidth()/2) * scaleFactor, (cocos2d::Director::getInstance()->getWinSize().height/2 - speedMatchCardLayer->getHeight()/2) * scaleFactor);
    
    scene->addChild(speedMatchCardLayer, 1);
    
    SpeedMatchUILayer* speedMatchUILayer = SpeedMatchUILayer::create();
    scene->addChild(speedMatchUILayer, 2);
    
    SpeedMatchStatLayer* speedMatchStatLayer = SpeedMatchStatLayer::create();
    scene->addChild(speedMatchStatLayer, 3);
    
    return scene;
}
