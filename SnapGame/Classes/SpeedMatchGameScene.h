//
//  SpeedMatchGameScene.h
//  SnapGame
//
//  Created by Sascha Becker on 06.12.14.
//
//

#ifndef __SnapGame__SpeedMatchGameScene__
#define __SnapGame__SpeedMatchGameScene__

#include "cocos2d.h"

class SpeedMatchGameScene : public cocos2d::Layer{
public:
    virtual bool init();
    static cocos2d::Scene* createScene();
    
    CREATE_FUNC(SpeedMatchGameScene);
};

#endif /* defined(__SnapGame__SpeedMatchGameScene__) */
