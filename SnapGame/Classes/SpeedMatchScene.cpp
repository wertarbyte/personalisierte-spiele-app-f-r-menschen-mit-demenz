#include "SpeedMatchScene.h"
#include "CustomImageManager.h"

USING_NS_CC;

int currentId = -1;
int lastId = -1;
int currentRound = 0;
std::vector<int> seed;
int numberOfCards = 100;
Sprite* correctOrWrong;

Scene* SpeedMatchScene::createScene()
{
    auto scene = Scene::create();
    auto layer = SpeedMatchScene::create();
    scene->addChild(layer);
    return scene;
}

bool SpeedMatchScene::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();    

    auto bg = Sprite::create("slice_08.jpg");
    
    auto s_visibleRect = Director::getInstance()->getOpenGLView()->getVisibleRect();
    float scaleY = s_visibleRect.size.height/bg->getContentSize().height;
    float scaleX = s_visibleRect.size.width/bg->getContentSize().width;
    if(scaleX>scaleY){
        bg->setScale(scaleX);
    }else{
        bg->setScale(scaleY);
    }
    bg->setPosition(Point(s_visibleRect.origin.x+s_visibleRect.size.width/2, s_visibleRect.origin.y+s_visibleRect.size.height/2));
    this->addChild(bg, -1);
    
    // generate Seed
    
    int numberOfUniqueCards = CustomImageManager::getInstance()->getCustomImagesLength();
    for (int i = 0; i < numberOfCards; i++) {
        int rand = random(0, numberOfUniqueCards - 1);
        seed.push_back(rand);
        CCLOG("Random number %i",rand);
    }
    
    int cardSize = 500;
    currentId = seed[currentRound];
    

    correctOrWrong = Sprite::create("correct.png");
    correctOrWrong->setPosition(visibleSize.width/2, visibleSize.height/2);// visibleSize.height - 100 - correctOrWrong->getContentSize().height);
   // correctOrWrong->setVisible(false);
    this->addChild(correctOrWrong,3);
    
    auto cardL = Sprite::create("yellow.jpg", cocos2d::Rect(0,0,cardSize, cardSize));
    cardL->setPosition(origin.x + visibleSize.width/4 + cardL->getContentSize().width/2,origin.y + visibleSize.height/2);
    this->addChild(cardL,2);
    
    cardR = Sprite::create(cocos2d::FileUtils::sharedFileUtils()->getWritablePath() + CustomImageManager::getInstance()->getCustomImage(currentId).getFilename(), cocos2d::Rect(0,0,1024, 1024));
    cardR->setScale(1.0 - (cardSize/1024.0) - 0.02);
    cardR->setPosition(origin.x + visibleSize.width/4*3 - cardSize/2,origin.y + visibleSize.height/2);
    this->addChild(cardR,1);
    
    //auto startButton = MenuItemImage::create("addNewImage.jpg", "addNewImage.jpg", CC_CALLBACK_1(SpeedMatchScene::clickStart, this));
    auto isNewButton = MenuItemImage::create("NewNormal.png", "NewSelected.png", CC_CALLBACK_1(SpeedMatchScene::clickisNew, this));
    auto isOldButton = MenuItemImage::create("OldNormal.png", "OldSelected.png", CC_CALLBACK_1(SpeedMatchScene::clickisOld, this));
    auto isNeworOldMenu = Menu::create(isNewButton, isOldButton, NULL);
    isNeworOldMenu->alignItemsHorizontallyWithPadding(50);
    isNeworOldMenu->setPosition(origin.x + visibleSize.width/2, origin.y + isOldButton->getContentSize().height + 5);
    this->addChild(isNeworOldMenu,2);
    
    return true;
}

void SpeedMatchScene::clickStart(cocos2d::Ref* pSender){
    ((MenuItemImage*)pSender)->setVisible(false);
    finalizeRound();
}

void SpeedMatchScene::clickisNew(cocos2d::Ref* pSender){
    if(lastId != currentId){
        CCLOG("richtig! is neu");
        correctOrWrong->setTexture("correct.png");
    }
    else{
        CCLOG("falsch! ist alt!!");
        correctOrWrong->setTexture("wrong.png");
    }
    
    Action* action = Sequence::create(FadeIn::create(0.1),FadeOut::create(0.5), NULL);
    correctOrWrong->runAction(action);
    
    finalizeRound();
}

void SpeedMatchScene::clickisOld(cocos2d::Ref* pSender){
    if(lastId == currentId){
        CCLOG("richtig! is alt");
        correctOrWrong->setTexture("correct.png");
    }
    else{
        CCLOG("falsch! ist neu!!");
        correctOrWrong->setTexture("wrong.png");
    }
    
    Action* action = Sequence::create(FadeIn::create(0.1),FadeOut::create(0.5), NULL);
    correctOrWrong->runAction(action);
    
    finalizeRound();
}

void SpeedMatchScene::finalizeRound(){
    currentRound++;
    if(currentRound < numberOfCards){
        lastId = currentId;
        currentId = seed[currentRound];
        
        Action* action = Sequence::create(MoveBy::create(0.05, cocos2d::Vec2(-500,0)), FadeOut::create(0.05), MoveBy::create(0, cocos2d::Vec2(500,0)), FadeIn::create(0.1), NULL);
        cardR->runAction(action);
        
        cardR->setTexture(cocos2d::FileUtils::sharedFileUtils()->getWritablePath() + CustomImageManager::getInstance()->getCustomImage(currentId).getFilename());
    }
    else
        CCLOG("Game over!");
}


