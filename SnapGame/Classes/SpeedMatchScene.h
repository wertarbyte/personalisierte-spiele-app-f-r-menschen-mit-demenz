#ifndef SnapGame_SPEEDMATCHSCENE_h
#define SnapGame_SPEEDMATCHSCENE_h

#include "cocos2d.h"

class SpeedMatchScene : public cocos2d::Layer
{
public:
    static cocos2d::Scene* createScene();
    virtual bool init();
    
    CREATE_FUNC(SpeedMatchScene);
private:
    cocos2d::Sprite* cardR;
    void clickStart(cocos2d::Ref* pSender);
    void clickisNew(cocos2d::Ref* pSender);
    void clickisOld(cocos2d::Ref* pSender);
    void finalizeRound();
};

#endif
