//
//  SpeedMatchStatLayer.cpp
//  SnapGame
//
//  Created by Sascha Becker on 09.12.14.
//
//

#include "SpeedMatchStatLayer.h"
#include "SpeedMatchGameController.h"

USING_NS_CC;

bool SpeedMatchStatLayer::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    
    SpeedMatchGameController::getInstance()->setStatDelegate(this);
    
    return true;
}

void SpeedMatchStatLayer::update(){
    removeAllChildren();
    stats = Label::createWithTTF("Runde " + std::to_string(SpeedMatchGameController::getInstance()->getCurrentRound() + 1) + " von " + std::to_string(SpeedMatchGameController::getInstance()->getRounds()) + "\n" + "Richtig:" + std::to_string(SpeedMatchGameController::getInstance()->getCountCorrect()) + " Falsch:" + std::to_string(SpeedMatchGameController::getInstance()->getCountWrong()), "LinLibertine_R.ttf", 50);
    
    stats->setPosition(Director::getInstance()->getWinSize().width - stats->getContentSize().width/2 - 10,Director::getInstance()->getWinSize().height - stats->getContentSize().height/2 - 10);
    
    this->addChild(stats);
}