//
//  SpeedMatchStatLayer.h
//  SnapGame
//
//  Created by Sascha Becker on 09.12.14.
//
//

#ifndef __SnapGame__SpeedMatchStatLayer__
#define __SnapGame__SpeedMatchStatLayer__

#include "cocos2d.h"

class SpeedMatchStatLayer : public cocos2d::Layer{
private:
    cocos2d::Label* stats;
    
public:
    virtual bool init();
    void update();
    
    CREATE_FUNC(SpeedMatchStatLayer);
};

#endif /* defined(__SnapGame__SpeedMatchStatLayer__) */
