//
//  SpeedMatchUILayer.cpp
//  SnapGame
//
//  Created by Sascha Becker on 09.12.14.
//
//

#include "SpeedMatchUILayer.h"
#include "SpeedMatchGameController.h"

USING_NS_CC;

bool SpeedMatchUILayer::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    
    SpeedMatchGameController::getInstance()->setUIDelegate(this);
    
    title = Sprite::create("title_speedMatch_rememberImages.png");
    auto titleMenuItem = MenuItemSprite::create(title, NULL, NULL);
    auto titleMenu = Menu::create(titleMenuItem, NULL);
    titleMenu->setPosition(Director::getInstance()->getWinSize().width/2,Director::getInstance()->getWinSize().height - title->getContentSize().height/2);
    this->addChild(titleMenu,1);
    
    auto actionSpriteNormal = Sprite::create("buttonGoOnNormal.png");
    auto actionSpritePressed = Sprite::create("buttonGoOnPressed.png");
    auto actionMenuItem = MenuItemSprite::create(actionSpriteNormal, actionSpritePressed, CC_CALLBACK_1(SpeedMatchUILayer::clickAction, this));
    
    auto gameListMenu = Menu::create(actionMenuItem, NULL);
    gameListMenu->setPosition(Director::getInstance()->getWinSize().width/2,actionSpriteNormal->getContentSize().height/2 + 10);
    this->addChild(gameListMenu,1);
    
    return true;
}

void SpeedMatchUILayer::clickAction(cocos2d::Ref* pSender){
    SpeedMatchGameController::getInstance()->tapGo();
}

void SpeedMatchUILayer::update(){
    if(SpeedMatchGameController::getInstance()->getState() == 1){
        title->setTexture("title_speedMatch_touchImages.png");
    }
}