//
//  SpeedMatchUILayer.h
//  SnapGame
//
//  Created by Sascha Becker on 09.12.14.
//
//

#ifndef __SnapGame__SpeedMatchUILayer__
#define __SnapGame__SpeedMatchUILayer__

#include "cocos2d.h"

class SpeedMatchUILayer : public cocos2d::Layer{
private:
    cocos2d::Sprite* title;
    void clickAction(cocos2d::Ref* pSender);
    
public:
    virtual bool init();
    void update();
    
    CREATE_FUNC(SpeedMatchUILayer);
};

#endif /* defined(__SnapGame__SpeedMatchUILayer__) */
