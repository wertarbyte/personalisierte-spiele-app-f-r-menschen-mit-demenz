//
//  bg.cpp
//  SnapGame
//
//  Created by Sascha Becker on 28.11.14.
//
//

#include "bg.h"
#include "SceneController.h"

USING_NS_CC;

bool bg::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    auto backgroundSprite = Sprite::create("wood.png");
    
    auto s_visibleRect = Director::getInstance()->getOpenGLView()->getVisibleRect();
    float scaleY = s_visibleRect.size.height/backgroundSprite->getContentSize().height;
    float scaleX = s_visibleRect.size.width/backgroundSprite->getContentSize().width;
    if(scaleX>scaleY){
        backgroundSprite->setScale(scaleX);
    }else{
        backgroundSprite->setScale(scaleY);
    }
    backgroundSprite->setPosition(Point(s_visibleRect.origin.x+s_visibleRect.size.width/2, s_visibleRect.origin.y+s_visibleRect.size.height/2));
    this->addChild(backgroundSprite, -1);
    
    auto backButton = MenuItemImage::create("buttonBackNormal.png", "buttonBackNormal.png", CC_CALLBACK_1(bg::back, this));
    auto gameMenu = Menu::create(backButton, NULL);
    gameMenu->setPosition(backButton->getContentSize().width/2, visibleSize.height - backButton->getContentSize().height/2);
    this->addChild(gameMenu,1);
    
    return true;
}

void bg::back(cocos2d::Ref* pSender){
    SceneController::getInstance()->showGameList(this);
}