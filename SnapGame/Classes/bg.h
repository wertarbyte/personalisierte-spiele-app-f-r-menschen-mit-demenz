//
//  bg.h
//  SnapGame
//
//  Created by Sascha Becker on 28.11.14.
//
//

#ifndef __SnapGame__bg__
#define __SnapGame__bg__

#include "cocos2d.h"

class bg : public cocos2d::Layer{
private:
    void back(cocos2d::Ref* pSender);
    
public:
    // Method 'init' in cocos2d-x returns bool, instead of 'id' in cocos2d-iphone (an object pointer)
    virtual bool init();
    
    // preprocessor macro for "static create()" constructor ( node() deprecated )
    CREATE_FUNC(bg);
};

#endif /* defined(__SnapGame__bg__) */
